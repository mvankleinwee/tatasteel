﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Automator;

namespace TataSteel
{
    /// <summary>
    /// Interaction logic for PrinterPortSettingsWindow.xaml
    /// </summary>
    public partial class PrinterPortSettingsWindow : Window
    {
        public PrinterPortSettingsWindow()
        {
            InitializeComponent();
        }
        public MainWindow mainWindow { get; set; }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (cbBaudRate.Text.Length != 0 && cbComPort.Text.Length != 0)
            {
                if (AutomatorCommFunctions.PrinterConnect(cbComPort.Text, Convert.ToInt32(cbBaudRate.Text), AutomatorCommFunctions.DataBits))
                {
                    AutomatorCommFunctions.PortName = cbComPort.Text;
                    AutomatorCommFunctions.BaudRate = Convert.ToInt32(cbBaudRate.Text);
                    mainWindow.txtCommPort.Text = cbComPort.Text;
                    mainWindow.txtBaudRate.Text = cbBaudRate.Text;
                    this.Close();
                }
                else
                    MessageBox.Show("Given port settings will not be able to connect the printer", "Warning");
            }
            else
                MessageBox.Show("Fill in all the fields", "Warning");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtDataBit_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
