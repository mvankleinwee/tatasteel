﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Automator;
using CellRO;
using CellroEventLogger;

namespace TataSteel
{
    /// <summary>
    /// Interaction logic for PositionSettingsWindow.xaml
    /// </summary>
    public partial class PositionSettingsWindow : Window
    {
        public PositionSettingsWindow()
        {
            InitializeComponent();
            DBContextDataContext db = new DBContextDataContext();
            var query = from i in db.Variables
                        where i.VariableName == "PrinterXPos" || i.VariableName == "PrinterYPos" || i.VariableName == "PrinterHeight"
                        select i;

            foreach (var i in query)
            {
                if (i.VariableName.Contains("PrinterXPos"))
                {
                    FirstX.Text = Convert.ToString(i.Value);
                }
                if (i.VariableName.Contains("PrinterYPos"))
                {
                    FirstY.Text = Convert.ToString(i.Value);
                }
                if (i.VariableName.Contains("PrinterHeight"))
                {
                    FirstZ.Text = Convert.ToString(i.Value);
                }
            }
            db = null;
        }
        public MainWindow mainWindow { get; set; }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            EventLogger.EventLog("Saved!!!");
            if (FirstX.Text.Length != 0 && FirstY.Text.Length != 0 && FirstZ.Text.Length != 0 )
            {
                DBContextDataContext db = new DBContextDataContext();
                Variable Va = new Variable();

                var query = from i in db.Variables
                            where i.VariableName == "PrinterXPos" || i.VariableName == "PrinterYPos" || i.VariableName == "PrinterHeight"
                            select i;

                foreach (var i in query)
                {
                    if (i.VariableName.Contains("PrinterXPos"))
                    {
                        i.Value = Convert.ToInt16(FirstX.Text);
                        db.SubmitChanges();
                    }
                    if (i.VariableName.Contains("PrinterYPos"))
                    {
                        i.Value = Convert.ToInt16(FirstY.Text);
                        db.SubmitChanges();
                    }
                    if (i.VariableName.Contains("PrinterHeight"))
                    {
                        i.Value = Convert.ToInt16(FirstZ.Text);
                        db.SubmitChanges();
                    }
                }
                db = null;
                
                 this.Close();
            }
            else
                MessageBox.Show("All the fields need a value", "Warning");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void FirstX_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !("D1D2D3D4D5D6D7D8D9D0NumPad0NumPad1NumPad2NumPad3NumPad4NumPad5NumPad6NumPad7NumPad8NumPad9".Contains(e.Key.ToString()));
            if (e.Handled == false)
            {
                e.Handled = ("DPN".Contains(e.Key.ToString()));
            }
        }

        private void FirstY_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !("D1D2D3D4D5D6D7D8D9D0NumPad0NumPad1NumPad2NumPad3NumPad4NumPad5NumPad6NumPad7NumPad8NumPad9".Contains(e.Key.ToString()));
            if (e.Handled == false)
            {
                e.Handled = ("DPN".Contains(e.Key.ToString()));
            }
        }

        private void FirstZ_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !("D1D2D3D4D5D6D7D8D9D0NumPad0NumPad1NumPad2NumPad3NumPad4NumPad5NumPad6NumPad7NumPad8NumPad9".Contains(e.Key.ToString()));
            if (e.Handled == false)
            {
                e.Handled = ("DPN".Contains(e.Key.ToString()));
            }
        }


    }
}
