﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using IDReader;
using Cognex.Cnx.Common;
using Cognex.DataMan.Discovery;
using Cognex.DataMan.SDK;
//using Cognex.DataMan.Utils;

namespace TataSteel
{
    /// <summary>
    /// Interaction logic for ScannerSettings.xaml
    /// </summary>
    public partial class ScannerSettings : Window
    {
       
        public ScannerSettings()
        {
            InitializeComponent();
        }

        private SynchronizationContext _syncContext = new SynchronizationContext();
        private EthSystemDiscoverer _ethSystemDiscoverer = null;
        private SerSystemDiscoverer _serSystemDiscoverer = null;
        private ISystemConnector _connector = null;
        private DataManSystem _system = null;

        public static EthSystemDiscoverer.SystemInfo _systemInfo = null;
        public static bool _enableConnect = false;

        public static bool EthSys = false;
        public static bool SerSys = false;
        //MainWindow mw = new MainWindow();

        //----------------------------------------------------------------------------------------
        public void ScanSetWind_OnLoad(object sender, RoutedEventArgs e)
        {
            _ethSystemDiscoverer = null;
            ListDevices();
        }

        public void ListDevices()
        {
            _ethSystemDiscoverer = null;
            // Create discoverers to discover ethernet and serial port systems.
            _ethSystemDiscoverer = new EthSystemDiscoverer();
            //_serSystemDiscoverer = new SerSystemDiscoverer();

            // Subscribe to the system discoved event.
            _ethSystemDiscoverer.SystemDiscovered += new EthSystemDiscoverer.SystemDiscoveredHandler(OnEthSystemDiscovered);
            //_serSystemDiscoverer.SystemDiscovered += new SerSystemDiscoverer.SystemDiscoveredHandler(OnSerSystemDiscovered);

            // Ask the discoverers to start discovering systems.
            _ethSystemDiscoverer.Discover();
            //_serSystemDiscoverer.Discover();
        }

        //---------- Services---------------------------------
        public void OnEthSystemDiscovered(EthSystemDiscoverer.SystemInfo systemInfo)
        {
            _syncContext.Post(
                 new SendOrPostCallback(
                     delegate
                     {
                         EthSys = true;
                         _systemInfo = systemInfo;
                         listBoxDetectedSystems.Dispatcher.BeginInvoke(new Action(() => listBoxDetectedSystems.Items.Add(systemInfo)));
                     }),
                     null);


        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            listBoxDetectedSystems.Items.Clear();
            _enableConnect = (_connector != null && !_connector.IsConnected);
            _ethSystemDiscoverer.Discover();
        }

        private void listBoxDetectedSystems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (listBoxDetectedSystems.SelectedIndex != -1 && listBoxDetectedSystems.Items.Count > listBoxDetectedSystems.SelectedIndex)
            {
                var system_info = listBoxDetectedSystems.Items[listBoxDetectedSystems.SelectedIndex];

                if (system_info is EthSystemDiscoverer.SystemInfo)
                {
                    EthSystemDiscoverer.SystemInfo eth_system_info = system_info as EthSystemDiscoverer.SystemInfo;
                    txtDeviceIP.Text = eth_system_info.IPAddress.ToString();
                    IDReaderCommFunctions.ScannerDeviceID = eth_system_info.Name;
                }
                _enableConnect = (_connector != null && !_connector.IsConnected) || (_connector == null);
            }
            else
                _enableConnect = false;

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

            IDReaderCommFunctions.ScannerIPAddress = txtDeviceIP.Text;
            IDReaderCommFunctions.ScannerUserID = txtUserID.Text ;
            IDReaderCommFunctions.ScannerPwd = txtPaswd.Text;

            //mw.txtIDRDeviceIP.Text = IDReaderCommFunctions.ScannerIPAddress;
            //mw.txtIDRDeviceName.Text = IDReaderCommFunctions.ScannerDeviceID;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (IDReaderCommFunctions.ScannerIPAddress == "" && IDReaderCommFunctions.ScannerDeviceID != "")
            {
                IDReaderCommFunctions.ScannerDeviceID = "";
            }
            //mw.txtIDRDeviceIP.Text = IDReaderCommFunctions.ScannerIPAddress;
            //mw.txtIDRDeviceName.Text = IDReaderCommFunctions.ScannerDeviceID;
            this.Close();
            sender = null;
        }
    }
}
