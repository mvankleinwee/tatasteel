﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Robot;

namespace TataSteel
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class RobotIPSettings : Window
    {
        public RobotIPSettings()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtIPAdress.Text.Length != 0)
            {
                if (RobotCommFunctions.RobotConnect(txtIPAdress.Text) == true)
                {
                    RobotCommFunctions.RobotIPAddress = txtIPAdress.Text;
                    RobotCommFunctions.RobotDisconnected();
                    this.Close();
                }
                else
                    MessageBox.Show("Given IP adress will not be able to connect the robot", "Warning");
            }
            else
                MessageBox.Show("Fill in all the fields", "Warning");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
