﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows;
using System.Xml;
using System.Diagnostics;
using System;
using CellRO;
using Cognex.DataMan.Discovery;
using Cognex.DataMan.SDK;
using Cognex.Cnx.Common;
//using Cognex.DataMan.Utils;  //explicitly added for GUI
using IDReader;
using Automator;
using Robot;

namespace TataSteel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //public PrinterPortSettingsWindow _PrinterPortSettingsWindow = new PrinterPortSettingsWindow();
        //MainWindow _MainWindow = new MainWindow(); 

        bool FailedComm = false;
        bool RobotCycleBusy = false;
        

        public MainWindow()
        {
            InitializeComponent();
            InitializePrinterCycle();
            InitializeScannerCycle();
            InitializeRobotCycle();
            DelayTimer(1, "Windows");  // Timer that triggers (Refreshes all the signals's value with database) every 3 seconds
            DelayTimer(1, "PrinterReceivedBuffer");
            DelayTimer(1, "ScannerTrigger");
            
        }

        

#region Printer
        //*****************************************************************************************************************************//
        //*******************************************PRINTER INTERFACE TAB*************************************************************//
        //*****************************************************************************************************************************//

        FlowDocument mcFlowDoc = new FlowDocument();
        Paragraph para = new Paragraph();
        SerialPort serial = new SerialPort();
        string recieved_data;

        private void Connect_Comms(object sender, RoutedEventArgs e)
        {
                System.Windows.Controls.Button connectDisconnect = (System.Windows.Controls.Button)sender;

                if (connectDisconnect.Name == "btnPrinterConnect")
                {
                    if (AutomatorCommFunctions.PrinterConnect(AutomatorCommFunctions.PortName, AutomatorCommFunctions.BaudRate, AutomatorCommFunctions.DataBits))
                    {
                        btnPrinterDisConnect.IsEnabled = true;
                        btnSend.IsEnabled = true;
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Failed to connect to printer, check connection.", "Alarm");
                    }
                }
                else
                {
                    AutomatorCommFunctions.PrinterDisConnect();
                }
        }

        private delegate void UpdateUiTextDelegate(string text);

        private void Recieve(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            recieved_data = serial.ReadExisting();
            Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData), recieved_data);
        }

        private void WriteData(string text)
        {
            // Assign the value of the recieved_data to the RichTextBox.
            para.Inlines.Add(text);
            mcFlowDoc.Blocks.Add(para);
            rtxtRecievedData.Document = mcFlowDoc;
        }

        private void Send_Data(object sender, RoutedEventArgs e)
        {
            string sendMessage = new TextRange(rtxtSendData.Document.ContentStart, rtxtSendData.Document.ContentEnd).Text;
            if (sendMessage.Length != 0)
            {
                AutomatorCommFunctions.PrintMessage(sendMessage);
                sendMessage = null;
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Type in some commands,Please. ", "Warning");
                sendMessage = null;
            }

            rtxtSendData.Document.Blocks.Clear();
        }

        public void ReceivedStringRefresher_Tick(object sender, EventArgs e)
        {
            if (AutomatorCommFunctions.StringAtReceivedBuffer.Length != 0)
            {
                if (para.Inlines.Count <= 3)
                {
                    para.Inlines.Add(AutomatorCommFunctions.StringAtReceivedBuffer);
                    mcFlowDoc.Blocks.Add(para);
                    rtxtRecievedData.Document = mcFlowDoc;
                }
                else rtxtRecievedData.Document.Blocks.Clear();
            }
        }

        /*********************PRINTER CYCLE BUTTONS*******************************************/
        private void InitializePrinterCycle()
        {
            //AutomatorCommFunctions.ResetPrinterSignals();
            //btnPrinterPause.IsEnabled = false;
            //btnPrinterStart.IsEnabled = true;
            //btnPrinterResume.IsEnabled = false;
            //btnPrinterStop.IsEnabled = false;
            //txtCommPort.Text = AutomatorCommFunctions.PortName;
            //txtBaudRate.Text = AutomatorCommFunctions.BaudRate.ToString();
            //btnPrinterDisConnect.IsEnabled = false;
            //btnSend.IsEnabled = false;

            Thread printThread = new Thread(CellROCommFunctions.PrinterCycle);

            AutomatorCommFunctions.ResetPrinterSignals();
            AutomatorCommFunctions.PrinterConnect(AutomatorCommFunctions.PortName, AutomatorCommFunctions.BaudRate, AutomatorCommFunctions.DataBits);
            printThread.Start();
            AutomatorCommFunctions.PrinterStarted = true;
            btnPrinterStart.IsEnabled = false;
            btnPrinterStop.IsEnabled = true;
            btnPrinterPause.IsEnabled = true;
            btnPrinterResume.IsEnabled = false;
            btnPrinterConnect.IsEnabled = false;
            btnPrinterDisConnect.IsEnabled = false;
            btnSend.IsEnabled = false;
            txtCommPort.Text = AutomatorCommFunctions.PortName;
            txtBaudRate.Text = AutomatorCommFunctions.BaudRate.ToString();

        }

        private void btnPrinterPauseResume_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button pauseResume = (System.Windows.Controls.Button)sender;
            if (pauseResume.Name == "btnPrinterPause")
            {
                if (!AutomatorCommFunctions.PrinterBusyStatus)
                {
                    AutomatorCommFunctions.PrinterPaused = true;
                    btnPrinterPause.IsEnabled = false;
                    btnPrinterResume.IsEnabled = true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Printer is busy.Please try after some time", "Warning");
                }
            }
            if (pauseResume.Name == "btnPrinterResume")
            {
                AutomatorCommFunctions.PrinterPaused = false;
                btnPrinterPause.IsEnabled = true;
                btnPrinterResume.IsEnabled = false;
            }
        }

        private void btnPrinterReset_Click(object sender, RoutedEventArgs e)
        {
            if ((AutomatorCommFunctions.PrinterPaused && !RobotSignals.ReqPrint) || !AutomatorCommFunctions.PrinterStarted)
            {
                AutomatorCommFunctions.ResetPrinterSignals();
                txtCommPort.Text = AutomatorCommFunctions.PortName;
                txtBaudRate.Text = AutomatorCommFunctions.BaudRate.ToString();
            }
            else
                System.Windows.Forms.MessageBox.Show("Printer cycle has to be paused and reset the ReqPrint if HIGH.", "Warning");
        }

        private void btnPrinterStartStop_Click(object sender, RoutedEventArgs e)
        {
            Thread printThread = new Thread(CellROCommFunctions.PrinterCycle);
            System.Windows.Controls.Button startStop = (System.Windows.Controls.Button)sender;
            bool manualCnxn = false;

            if (startStop.Name == "btnPrinterStart")
            {
                if (btnPrinterDisConnect.IsEnabled)
                {
                        manualCnxn = false;
                        btnPrinterDisConnect.IsEnabled = false;
                        btnSend.IsEnabled = false;
                    
                }
                if (!manualCnxn)
                {
                    if (AutomatorCommFunctions.PrinterConnect(AutomatorCommFunctions.PortName, AutomatorCommFunctions.BaudRate, AutomatorCommFunctions.DataBits))
                    {
                        AutomatorCommFunctions.ResetPrinterSignals();
                        printThread.Start();
                        AutomatorCommFunctions.PrinterStarted = true;
                        btnPrinterStart.IsEnabled = false;
                        btnPrinterStop.IsEnabled = true;
                        btnPrinterPause.IsEnabled = true;
                        btnPrinterResume.IsEnabled = false;
                        btnSend.IsEnabled = false;
                    }
                    else
                    { System.Windows.Forms.MessageBox.Show("Failed to connect to printer, check connection.", "Alarm"); }
                }
            }

            if (startStop.Name == "btnPrinterStop")
            {
                if (!AutomatorCommFunctions.PrinterBusyStatus)
                {
                    manualCnxn = true;
                    if (System.Windows.Forms.MessageBox.Show("Are you sure you want to stop the PrinterCycle?", "Warning", MessageBoxButtons.YesNo) == (DialogResult)MessageBoxResult.Yes)
                    {
                        //printThread.Abort();
                        AutomatorCommFunctions.PrinterStarted = false;
                        AutomatorCommFunctions.ResetPrinterSignals();
                        //printThread = null;
                        btnPrinterStop.IsEnabled = false;
                        btnPrinterStart.IsEnabled = true;
                        btnPrinterPause.IsEnabled = false;
                        btnPrinterResume.IsEnabled = false;
                        if (AutomatorCommFunctions.PrinterPortOpen == true)
                        {
                            btnPrinterDisConnect.IsEnabled = true;
                            btnSend.IsEnabled = true;
                        }
                        else
                        {
                            btnPrinterConnect.IsEnabled = true;
                        } 
                    }                   
                }
                else
                    System.Windows.Forms.MessageBox.Show("Printer is busy.Please try after some time.", "Warning");
            }
        }

        private void btnPrinterSettings_Click(object sender, RoutedEventArgs e)
        {
            if ((!AutomatorCommFunctions.PrinterStarted))
            {
                PrinterPortSettingsWindow _PrinterPortSettingsWindow = new PrinterPortSettingsWindow();
                if (!_PrinterPortSettingsWindow.IsVisible)
                {
                    _PrinterPortSettingsWindow.mainWindow = this;
                    _PrinterPortSettingsWindow.ShowDialog();
                    _PrinterPortSettingsWindow.Activate();
                }
                else
                {
                    _PrinterPortSettingsWindow.Close();
                    _PrinterPortSettingsWindow = null;
                }
            }
            else
                System.Windows.Forms.MessageBox.Show("Printercycle has to be stopped, to edit the settings.", "Warning");

        }

        private void btnXYZSettings_Click(object sender, RoutedEventArgs e)
        {
            if ((!AutomatorCommFunctions.PrinterStarted))
            {
                PositionSettingsWindow _PositionSettingsWindow = new PositionSettingsWindow();
                if (!_PositionSettingsWindow.IsVisible)
                {
                    _PositionSettingsWindow.mainWindow = this;
                    _PositionSettingsWindow.ShowDialog();
                    _PositionSettingsWindow.Activate();
                }
                else
                {
                    _PositionSettingsWindow.Close();
                    _PositionSettingsWindow = null;
                }
            }
            else
                System.Windows.Forms.MessageBox.Show("Printercycle has to be stopped, to edit the settings.", "Warning");
        }
#endregion

        /*************************************************************************************/

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //*****************************************************************************************************************************//
        //*******************************************MAIN INTERFACE TAB****************************************************************//
        //*****************************************************************************************************************************//

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void WriteToEventLog(string message)
        {
            string cs = "Cellro";
            EventLog elog = new EventLog();
            if (!EventLog.SourceExists(cs))
            {
                EventLog.CreateEventSource(cs, cs);
            }
            elog.Source = cs;
            elog.EnableRaisingEvents = true;
            elog.WriteEntry(message);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //*****************************************************************************************************************************//
        //*******************************************ROBOT INTERFACE TAB***************************************************************//
        //*****************************************************************************************************************************//

#region Timer
        public void DelayTimer(int secs, string Caller)
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            switch (Caller)
            {
                case "Windows": dispatcherTimer.Tick += new EventHandler(WindowsRefresh_Tick);
                    break;
                case "PrinterReceivedBuffer": dispatcherTimer.Tick += new EventHandler(ReceivedStringRefresher_Tick);
                    break;
                case "ScannerTrigger": dispatcherTimer.Tick += new EventHandler(ScannerTriggerRefresher_Tick);
                    break;
                default:
                    break;
            }
            dispatcherTimer.Interval = new TimeSpan(0, 0, secs);
            dispatcherTimer.Start();
        }
        #endregion

        public void WindowsRefresh_Tick(object sender, EventArgs e)
        {
            try
            {
                RobotCycle();
                WindowsRefreshCycle();
            }
            catch (Exception ex)
            {
                WriteToEventLog("Thrown exception RobotCycle or Windowsrefresh: " + ex);
            }
        }

# region ClickEvents
        private void EclReqAlive_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqAlive) RobotSignals.ReqAlive = true; else RobotSignals.ReqAlive = false; }
        private void EclReqPrint_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqPrint) RobotSignals.ReqPrint = true; else RobotSignals.ReqPrint = false; }
        private void EclReqScanFirstTime_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqScanFirstTime) RobotSignals.ReqScanFirstTime = true; else RobotSignals.ReqScanFirstTime = false; }
        private void EclReqScanPrinted_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqScanPrinted) RobotSignals.ReqScanPrinted = true; else RobotSignals.ReqScanPrinted = false; }
        private void EclReqDeleteItem_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqDeleteItem) RobotSignals.ReqDeleteItem = true; else RobotSignals.ReqDeleteItem = false; }
        private void EclReqDeleteAll_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqDeleteAll) RobotSignals.ReqDeleteAll = true; else RobotSignals.ReqDeleteAll = false; }
        private void EclReqReadMeasured_MouseClick(object sender, MouseButtonEventArgs e) { if (!RobotSignals.ReqReadMeasuredValue) RobotSignals.ReqReadMeasuredValue = true; else RobotSignals.ReqReadMeasuredValue = false; }

        private void EclAckAlive_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckAlive) RobotSignals.AckAlive = true; else RobotSignals.AckAlive = false;
            }
        }
        private void EclAckPrintOK_MouseClick(object sender, MouseButtonEventArgs e) 
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckPrintOK) RobotSignals.AckPrintOK = true; else RobotSignals.AckPrintOK = false;
            }
        }
        private void EclAckPrintNOK_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckPrintNOK) RobotSignals.AckPrintNOK = true; else RobotSignals.AckPrintNOK = false;
            }
        }
        private void EclAckScanFirstTimeOK_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckScanFirstTimeOK) RobotSignals.AckScanFirstTimeOK = true; else RobotSignals.AckScanFirstTimeOK = false;
            }
        }
        private void EclAckScanFirstTimeNOK_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckScanFirstTimeNOK) RobotSignals.AckScanFirstTimeNOK = true; else RobotSignals.AckScanFirstTimeNOK = false;
            }
        }
        private void EclAckScanPrintedOK_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckScanPrintedOK) RobotSignals.AckScanPrintedOK = true; else RobotSignals.AckScanPrintedOK = false;
            }
        }
        private void EclAckScanPrintedNOK_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckScanPrintedNOK) RobotSignals.AckScanPrintedNOK = true; else RobotSignals.AckScanPrintedNOK = false;
            }
        }
        private void EclAckDeleteItem_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckDeleteItemOK) RobotSignals.AckDeleteItemOK = true; else RobotSignals.AckDeleteItemOK = false;
            }
        }
        private void EclAckDeleteAll_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckDeleteAllOK) RobotSignals.AckDeleteAllOK = true; else RobotSignals.AckDeleteAllOK = false;
            }
        }

        private void EclAckReadMeasured_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)
            {
                if (!RobotSignals.AckReadMeasuredValue) RobotSignals.AckReadMeasuredValue = true; else RobotSignals.AckReadMeasuredValue = false;
            }
        }

        #endregion

        private void WindowsRefreshCycle()
        {
            DBContextDataContext db = new DBContextDataContext();
            DB.ItemsSource = db.SampleDatas;
            db = null;
                        
            System.Windows.Media.Brush onColor = new SolidColorBrush(System.Windows.Media.Color.FromArgb(120, 0, 255, 0));
            System.Windows.Media.Brush offColor = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
            // fills in the color based on the Database values

            if (RobotSignals.ReqAlive) EclReqAlive.Fill = onColor; else EclReqAlive.Fill = offColor;
            if (RobotSignals.ReqPrint) EclReqPrint.Fill = onColor; else EclReqPrint.Fill = offColor;
            if (RobotSignals.ReqScanFirstTime) EclReqScanFirstTime.Fill = onColor; else EclReqScanFirstTime.Fill = offColor;
            if (RobotSignals.ReqScanPrinted) EclReqScanPrinted.Fill = onColor; else EclReqScanPrinted.Fill = offColor;
            if (RobotSignals.ReqDeleteItem) EclReqDeleteItem.Fill = onColor; else EclReqDeleteItem.Fill = offColor;
            if (RobotSignals.ReqDeleteAll) EclReqDeleteAll.Fill = onColor; else EclReqDeleteAll.Fill = offColor;
            if (RobotSignals.ReqReadMeasuredValue) EclReqReadMeasured.Fill = onColor; else EclReqReadMeasured.Fill = offColor;
            if (RobotSignals.AckAlive) EclAckAlive.Fill = onColor; else EclAckAlive.Fill = offColor;

            if (RobotSignals.AckPrintOK) EclAckPrintOK.Fill = onColor; else EclAckPrintOK.Fill = offColor;
            if (RobotSignals.AckPrintNOK) EclAckPrintNOK.Fill = onColor; else EclAckPrintNOK.Fill = offColor;
            if (RobotSignals.AckScanFirstTimeOK) EclAckScanFirstTimeOK.Fill = onColor; else EclAckScanFirstTimeOK.Fill = offColor;
            if (RobotSignals.AckScanFirstTimeNOK) EclAckScanFirstTimeNOK.Fill = onColor; else EclAckScanFirstTimeNOK.Fill = offColor;
            if (RobotSignals.AckScanPrintedOK) EclAckScanPrintedOK.Fill = onColor; else EclAckScanPrintedOK.Fill = offColor;
            if (RobotSignals.AckScanPrintedNOK) EclAckScanPrintedNOK.Fill = onColor; else EclAckScanPrintedNOK.Fill = offColor;
            if (RobotSignals.AckDeleteItemOK) EclAckDeleteItem.Fill = onColor; else EclAckDeleteItem.Fill = offColor;
            if (RobotSignals.AckDeleteAllOK) EclAckDeleteAll.Fill = onColor; else EclAckDeleteAll.Fill = offColor;
            if (RobotSignals.AckReadMeasuredValue) EclAckReadMeasured.Fill = onColor; else EclAckReadMeasured.Fill = offColor;

            if (RobotCommFunctions.RobotStarted) EclRobotStart.Fill = onColor; else EclRobotStart.Fill = offColor;
            if (RobotCommFunctions.RobotBusyStatus) EclRobotBusy.Fill = onColor; else EclRobotBusy.Fill = offColor;
            if (RobotCommFunctions.RobotPaused) EclRobotPause.Fill = onColor; else EclRobotPause.Fill = offColor;
            if (IDReaderCommFunctions.ScannerStarted) EclScannerStart.Fill = onColor; else EclScannerStart.Fill = offColor;
            if (IDReaderCommFunctions.ScannerBusyStatus) EclScannerBusy.Fill = onColor; else EclScannerBusy.Fill = offColor;
            if (IDReaderCommFunctions.ScannerPaused) EclScannerPause.Fill = onColor; else EclScannerPause.Fill = offColor;
            if (AutomatorCommFunctions.PrinterStarted) EclPrinterStart.Fill = onColor; else EclPrinterStart.Fill = offColor;
            if (AutomatorCommFunctions.PrinterBusyStatus) EclPrinterBusy.Fill = onColor; else EclPrinterBusy.Fill = offColor;
            if (AutomatorCommFunctions.PrinterPaused) EclPrinterPause.Fill = onColor; else EclPrinterPause.Fill = offColor;


            try
            {
                if (_system != null && PingHost(IDReaderCommFunctions.ScannerIPAddress))
                {
                    EclIDRON.Fill = onColor; EclIDROFF.Fill = offColor;
                    if (btnIDRConnect.IsEnabled) { AddListItem("System Connected"); }
                    btnIDRConnect.IsEnabled = false;
                }
                else
                {
                    if (!btnIDRConnect.IsEnabled)
                    {
                        EclIDRON.Fill = offColor; EclIDROFF.Fill = onColor;

                        btnScannerStop.IsEnabled = false;
                        btnScannerStart.IsEnabled = true;
                        btnScannerPause.IsEnabled = false;
                        btnScannerResume.IsEnabled = false;
                        btnIDRConnect.IsEnabled = true;
                        btnIDRDisconnect.IsEnabled = false;

                        IDReaderCommFunctions.ResetScannerSignals();
                        _system = null;
                        AddListItem("System Disconnected");

                        if (IDReaderCommFunctions.ScannerStarted)
                        {
                            Thread scanThread = new Thread(CellROCommFunctions.ScannerCycle);

                            //scanThread.Abort();
                            scanThread = null;
                            IDReaderCommFunctions.ScannerStarted = false;
                            System.Windows.Forms.MessageBox.Show("Connection lost with scanner", "Alarm");
                        }
                    }
                }

                if (RobotCommFunctions.RoConnected && PingHost(RobotCommFunctions.RobotIPAddress))
                {
                    EclRobotON.Fill = onColor; EclRobotOFF.Fill = offColor;
                }
                else
                {
                    if (!btnRobotConnect.IsEnabled)
                    {
                        EclRobotON.Fill = offColor; EclRobotOFF.Fill = onColor;

                        btnRobotConnect.IsEnabled = true;
                        btnRobotStop.IsEnabled = false;
                        btnRobotStart.IsEnabled = true;
                        btnRobotPause.IsEnabled = false;
                        btnRobotResume.IsEnabled = false;
                        btnRobotDisConnect.IsEnabled = false;
                        LblInfo.Text = "Disconnected";
                        RobotCommFunctions.RobotDisconnected();
                        if (RobotCommFunctions.RobotStarted)
                        {
                            RobotCommFunctions.RobotStarted = false;
                            System.Windows.Forms.MessageBox.Show("Connection lost with robot", "Alarm");
                        }
                    }
                }

                if (AutomatorCommFunctions.PrinterStillConnected())
                {
                    EclPrinterON.Fill = onColor; EclPrinterOFF.Fill = offColor;
                    btnPrinterConnect.IsEnabled = false;
                }
                else
                {
                    if (!btnPrinterConnect.IsEnabled)
                    {
                        EclPrinterON.Fill = offColor; EclPrinterOFF.Fill = onColor;

                        btnPrinterStop.IsEnabled = false;
                        btnPrinterStart.IsEnabled = true;
                        btnPrinterPause.IsEnabled = false;
                        btnPrinterResume.IsEnabled = false;
                        btnPrinterConnect.IsEnabled = true;
                        btnPrinterDisConnect.IsEnabled = false;
                        btnSend.IsEnabled = false;

                        AutomatorCommFunctions.ResetPrinterSignals();
                        AutomatorCommFunctions.printerPort = null;

                        if (AutomatorCommFunctions.PrinterStarted)
                        {
                            Thread printThread = new Thread(CellROCommFunctions.PrinterCycle);

                            //printThread.Abort();
                            printThread = null;
                            AutomatorCommFunctions.PrinterStarted = false;
                            System.Windows.Forms.MessageBox.Show("Connection lost with printer", "Alarm");
                        }
                    }
                }
            }
            catch (Exception ex)
            { WriteToEventLog("Thrown exception in the Check Connections, Thrown exception: " + ex); }
        }

#region Robot
        /*************************ROBOT CYCLE******************************************/
        private void RobotCycle()
        {
            bool exitcondition = false;
            bool exitcondition2 = false;
            
            DBContextDataContext db = new DBContextDataContext();
            
            SampleData sd = new SampleData();   //Local DB at CellRO side.

            exitcondition = !RobotCommFunctions.RobotStarted;
            if (FailedComm == true)
            {
                //RobotCommFunctions.RobotDisconnected();
                WriteToEventLog("Robot communication error, check robot connection before reconnecting!");
                FailedComm = false;
                exitcondition2 = true;              
            }

            if (!exitcondition && !exitcondition2 && !RobotCycleBusy)
            {
                RobotCycleBusy = true;

                if (RobotCommFunctions.RobotPaused == false)
                {
                    int[] Values = new int[14];
                    /************************************************************************************************************************/
                    //********Step1. Check if Connection with robot is up, otherwise connect.*****************
                    /************************************************************************************************************************/
                    if (RobotCommFunctions.RobotConnect(RobotCommFunctions.RobotIPAddress) == false)
                    {
                        //No connection, stop cycle ;
                        WriteToEventLog("Robot RobotConnect failed");
                        FailedComm = true;
                        RobotCycleBusy = false;
                        return;
                    }
                    LblInfo.Text = "Connected to " + RobotCommFunctions.RobotIPAddress;
                    /************************************************************************************************************************/
                    //********Step2. Getting requests and ItemID.*****************
                    /************************************************************************************************************************/
                    if (RobotCommFunctions.GetRequests() == false)
                    {
                        //Communication robot failed???
                        WriteToEventLog("Robot GetRequests failed");
                        FailedComm = true;
                        RobotCycleBusy = false;
                        return;
                    }

                    /************************************************************************************************************************/
                    //********Step3. Calling functions.*****************
                    /************************************************************************************************************************/
                    CellROCommFunctions.ReqAlive();

                    //20180511 MvK: Function to read measured value and save in File.
                    CellROCommFunctions.ReqLogMeasuredValue();

                    CellROCommFunctions.ReqDeleteItem();

                    CellROCommFunctions.ReqDeleteAll();

                    /************************************************************************************************************************/
                    //********Step4. Writing acknowledgements and ItemID.*****************
                    /************************************************************************************************************************/
                    if (RobotSignals.AckScanFirstTimeOK == true)
                    {
                        //Get Data to send to robot
                        var query = from ID in db.SampleDatas
                                    where ID.ItemID == RobotSignals.AckScanFirstTimeOK_ItemID  //must be int
                                    select ID;

                        foreach (var q in query)
                        {
                            Values[0] = q.ItemID;
                            Values[1] = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(0, 4));
                            Values[2] = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(4, 1));
                            Values[3] = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(5, 1));
                            Values[4] = q.Werkplaatsnummer;
                            Values[5] = q.ProefstukUitnameCode;
                            Values[6] = q.ProefIndex;
                            Values[7] = Convert.ToInt32(q.Status);
                            Values[8] = Convert.ToInt32(q.Orderdikte* 100);
                            Values[9] = Convert.ToInt32(q.TolBoven * 10);
                            Values[10] = Convert.ToInt32(q.TolOnder * 10);
                            Values[11] = Convert.ToInt32(q.FreesDikte * 10);
                            Values[12] = Convert.ToInt32(q.CodeRm);
                            Values[13] = Convert.ToInt32(q.BeproevingsTemperatuur);

                            //20180509 MvK: Remember values for "Mearurement logging"
                            RobotSignals.ItemID = q.ItemID;
                            RobotSignals.AskedThickness = Convert.ToDouble(q.Orderdikte);
                            RobotSignals.TolUnder = Convert.ToDouble(q.TolOnder);
                            RobotSignals.TolAbove = Convert.ToDouble(q.TolBoven);
                        }
                    }

                    if (RobotCommFunctions.WriteAcknowledgements(Values) == false) //, Values2
                    {
                        //Communication robot failed???
                        WriteToEventLog("Robot WriteAck Failed");
                        FailedComm = true;
                        RobotCycleBusy = false;
                        return;
                    }

                }
                RobotCycleBusy = false;
            }
        }

        /*************************ROBOT CYCLE BUTTONS******************************************/

        private void InitializeRobotCycle()
        {
            //RobotCommFunctions.ResetRobotSignals();
            //btnRobotPause.IsEnabled = false;
            //btnRobotStart.IsEnabled = true;
            //btnRobotResume.IsEnabled = false;
            //btnRobotStop.IsEnabled = false;
            //txtRobotDeviceIP.Text = RobotCommFunctions.RobotIPAddress;
            //btnRobotDisConnect.IsEnabled = false;

            RobotCommFunctions.ResetRobotSignals();
            RobotCommFunctions.RobotStarted = true;
            btnRobotStart.IsEnabled = false;
            btnRobotStop.IsEnabled = true;
            btnRobotPause.IsEnabled = true;
            btnRobotResume.IsEnabled = false;
            btnRobotConnect.IsEnabled = false;
            btnRobotDisConnect.IsEnabled = false;
            txtRobotDeviceIP.Text = RobotCommFunctions.RobotIPAddress;
        }

        private void btnRobotPauseResume_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button pauseResume = (System.Windows.Controls.Button)sender;
            if (pauseResume.Name == "btnRobotPause")
            {
                if (!RobotCommFunctions.RobotBusyStatus)
                {
                    RobotCommFunctions.RobotPaused = true;
                    btnRobotPause.IsEnabled = false;
                    btnRobotResume.IsEnabled = true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Robot is busy.Please try after some time", "Warning");
                }
            }
            if (pauseResume.Name == "btnRobotResume")
            {
                RobotCommFunctions.RobotPaused = false;
                btnRobotPause.IsEnabled = true;
                btnRobotResume.IsEnabled = false;
            }

        }

        private void btnRobotReset_Click(object sender, RoutedEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)       //taken out the part "RobotCommFunctions.RobotPaused ||", because when paused u don't want to reset signals. Resetting signals can cause critical errors!
                RobotCommFunctions.ResetRobotSignals();
            else
                System.Windows.Forms.MessageBox.Show("Robot cycle has to be Stopped.", "Warning");
        }

        private void btnRobotStartStop_Click(object sender, RoutedEventArgs e)
        {
            //Thread robotThread = new Thread(CellROCommFunctions.RobotCycle);
            System.Windows.Controls.Button startStop = (System.Windows.Controls.Button)sender;

            if (startStop.Name == "btnRobotStart")
            {
                //RobotCommFunctions.ResetRobotSignals();
                //robotThread.Start();
                if (RobotCommFunctions.RobotConnect(RobotCommFunctions.RobotIPAddress))
                {
                    RobotCommFunctions.RobotStarted = true;
                    btnRobotStart.IsEnabled = false;
                    btnRobotStop.IsEnabled = true;
                    btnRobotPause.IsEnabled = true;
                    btnRobotResume.IsEnabled = false;
                    btnRobotConnect.IsEnabled = false;
                    btnRobotDisConnect.IsEnabled = false;
                }
                else
                { System.Windows.Forms.MessageBox.Show("Failed to connect to robot, check connection.", "Alarm"); }
            }

            if (startStop.Name == "btnRobotStop")
            {
                if (!RobotCommFunctions.RobotBusyStatus)
                {
                    if (System.Windows.Forms.MessageBox.Show("Are you sure you want to stop the RobotCycle?", "Warning", MessageBoxButtons.YesNo) == (DialogResult)MessageBoxResult.Yes)
                    {
                        //robotThread.Abort();
                        RobotCommFunctions.RobotStarted = false;
                        RobotCommFunctions.RobotPaused = false;
                        //RobotCommFunctions.ResetRobotSignals();
                        //robotThread = null;
                        btnRobotStop.IsEnabled = false;
                        btnRobotStart.IsEnabled = true;
                        btnRobotPause.IsEnabled = false;
                        btnRobotResume.IsEnabled = false;
                        if (btnRobotDisConnect.IsEnabled == false)
                        {
                            btnRobotConnect.IsEnabled = false;
                            btnRobotDisConnect.IsEnabled = true;
                        }
                        else
                        {
                            btnRobotDisConnect.IsEnabled = false;
                        }
                    }                    
                }
                else
                    System.Windows.Forms.MessageBox.Show("Robot is busy.Please try after some time.", "Warning");
            }
        }

        private void btnRobotSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!RobotCommFunctions.RobotStarted)                           //taken out the part "RobotCommFunctions.RobotPaused ||", because when paused u don't want to reset signals. Resetting signals can cause critical errors!
            {
                //Robot connect setting windows should be opened
                    RobotIPSettings _RobotIPSettings = new RobotIPSettings();  
                    if (!_RobotIPSettings.IsVisible)
                    {
                        _RobotIPSettings.ShowDialog();
                        _RobotIPSettings.Activate();
                    }
                    else
                    {
                        _RobotIPSettings.Close();
                        _RobotIPSettings = null;
                    }
            }
            else
                System.Windows.Forms.MessageBox.Show("Robotcycle has to be Stopped, to edit the settings.", "Warning");

        }

        private void ConnectDisconnectRobot_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button connectDisconnect = (System.Windows.Controls.Button)sender;

            if (connectDisconnect.Name == "btnRobotConnect")
            {
                if (RobotCommFunctions.RobotConnect(RobotCommFunctions.RobotIPAddress) == true)
                {
                    LblInfo.Text = "Connected to " + RobotCommFunctions.RobotIPAddress;
                    btnRobotConnect.IsEnabled = false;
                    btnRobotDisConnect.IsEnabled = true;
                }
                else { System.Windows.Forms.MessageBox.Show("Failed to connect to robot, check the connection", "Alarm"); }
            }

            if (connectDisconnect.Name == "btnRobotDisConnect")
            {
                RobotCommFunctions.RobotDisconnected();
                LblInfo.Text = "Disconnected";
                btnRobotDisConnect.IsEnabled = false;
            }
        }

        /*************************************************************************************/
#endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#region Scanner
        //*****************************************************************************************************************************//
        //*******************************************SCANNER INTERFACE TAB*************************************************************//
        //*****************************************************************************************************************************//
        //-------------Declaration of ID Reader Variables
        private SynchronizationContext _syncContext = new SynchronizationContext();
        private ISystemConnector _connector = null;
        private DataManSystem _system = null;
        private DataManSystem _system1 = null;
        private object _currentResultInfoSyncLock = new object();
        private IDReader.IDReaderCommFunctions.ResultInfo _currentResultInfo = new IDReader.IDReaderCommFunctions.ResultInfo(-1, null, null, null);
        private bool _closing = false;
        private bool _autoconnect = false;
        private object _listAddItemLock = new object();

        public void ScannerTriggerRefresher_Tick(object sender, EventArgs e)
        {
            if (IDReaderCommFunctions.ScannerTriggered == "ON" && IDReaderCommFunctions.ScannedString.Length == 0 && (RobotSignals.ReqScanFirstTime || RobotSignals.ReqScanPrinted))
            {
                ClearScannedValues();
                if (ScannerConnect())
                {
                    _system.SendCommand("TRIGGER ON");
                    //Thread.Sleep(4000);
                    //_system.SendCommand("TRIGGER OFF");
                    //IDReaderCommFunctions.ScannerTriggered = "OFF";
                }
                else
                    System.Windows.Forms.MessageBox.Show("Scanner not connected.", "Error");
            }
            else
                if(_system != null && PingHost(IDReaderCommFunctions.ScannerIPAddress))
                { _system.SendCommand("TRIGGER OFF"); }
        }

        public bool ScannerConnect()
        {
            bool connectFlag = false;
            if (IDReaderCommFunctions.ScannerIPAddress.Length != 0)
            {
                IPAddress ip;
                if (_system != null)
                    if (_system.IsConnected) return true;
                try
                {
                    var system_info = ScannerSettings._systemInfo;
                    if (ScannerSettings.EthSys.Equals(true))
                    {
                        EthSystemDiscoverer.SystemInfo eth_system_info = system_info as EthSystemDiscoverer.SystemInfo;
                        ip = eth_system_info.IPAddress;
                    }
                    else
                    {
                        ip = IPAddress.Parse(IDReaderCommFunctions.ScannerIPAddress);
                    }

                    EthSystemConnector conn = new EthSystemConnector(ip);
                    conn.UserName = IDReaderCommFunctions.ScannerUserID;
                    conn.Password = IDReaderCommFunctions.ScannerPwd;
                    _connector = conn;
                    _system = new DataManSystem(_connector);
                    _system.DefaultTimeout = 5000;
                    
                    // Subscribe to events that are signalled when the deveice sends auto-responses.
                    _system.XmlResultArrived += new XmlResultArrivedHandler(OnXmlResultArrived);
                    _system.ImageArrived += new ImageArrivedHandler(OnImageArrived);
                    _system.ImageGraphicsArrived += new ImageGraphicsArrivedHandler(OnImageGraphicsArrived);
                    _system.Connect();
                    if (_system.IsConnected) connectFlag = true; IDReaderCommFunctions.ScannerConnected = true;
                }
                catch (Exception ex)
                {
                    AddListItem("Failed to connect Scanner: " + ex.ToString());
                    btnIDRConnect.IsEnabled = true;
                    return connectFlag;
                }
            }
            else
                System.Windows.Forms.MessageBox.Show("Please select the Device using Settings","Info");

            return connectFlag;
        }

        public void ScannerDisconnect()
        {
            _autoconnect = false;
            IDReaderCommFunctions.ScannerConnected = false;
            btnIDRDisconnect.IsEnabled = false;
            if (_system != null)
            {
                if (_system.IsConnected) _system.Disconnect();

                _system.XmlResultArrived -= new XmlResultArrivedHandler(OnXmlResultArrived);
                _system.ImageArrived -= new ImageArrivedHandler(OnImageArrived);
                _system.ImageGraphicsArrived -= new ImageGraphicsArrivedHandler(OnImageGraphicsArrived);
            }
                CleanupConnection();
        }

        private void CleanupConnection()
        {
            if (null != _system)
            {
                _system.XmlResultArrived -= OnXmlResultArrived;
                _system.ImageArrived -= OnImageArrived;
                _system.ImageGraphicsArrived -= OnImageGraphicsArrived;
            }
            _connector = null;
            _system = null;
        }    

        private void btnIDRConnect_Click(object sender, RoutedEventArgs e)
        {
            if (ScannerConnect())
            {
                btnIDRDisconnect.IsEnabled = true;
                btnTrigger.IsEnabled = true;
                _autoconnect = true;
            }
            else { System.Windows.Forms.MessageBox.Show("Failed to connect to scanner, check connection", "Alarm"); }
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            ScannerDisconnect();            
            btnIDRDisconnect.IsEnabled = false;
            btnTrigger.IsEnabled = false;
        }

        //------------------------------Services----------------------
        private void AddListItem(object item)
        {
            lock (_listAddItemLock)
            {
                listBox1.Dispatcher.BeginInvoke(new Action(() => listBox1.Items.Add(item)));
                if (listBox1.Items.Count > 5)
                    listBox1.Dispatcher.BeginInvoke(new Action(() => listBox1.Items.RemoveAt(0)));
                if (listBox1.Items.Count > 0)
                    listBox1.Dispatcher.BeginInvoke(new Action(() => listBox1.SelectedIndex = listBox1.Items.Count - 1));
            }
        }

        //public string ReadString()
        //{
        //    IPAddress ip1 = IPAddress.Parse(IDReaderCommFunctions.ScannerIPAddress);
        //    EthSystemConnector conn1 = new EthSystemConnector(ip1);
        //    conn1.UserName = IDReaderCommFunctions.ScannerUserID;
        //    conn1.Password = IDReaderCommFunctions.ScannerPwd;
        //    try
        //    {
        //        _system1 = new DataManSystem(conn1);
        //        _system1.XmlResultArrived += new XmlResultArrivedHandler(OnXmlResultArrived);
        //        _system1.Connect();
        //        return IDReaderCommFunctions.ScannedString;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (null != _system1)
        //        {
        //            _system1.XmlResultArrived -= new XmlResultArrivedHandler(OnXmlResultArrived);
        //        }

        //        conn1 = null;
        //        _system1 = null;
        //        return ex.ToString();
        //    }
        //}  

        private void OnXmlResultArrived(object sender, XmlResultArrivedEventArgs args)
        {
            //System.Windows.Forms.MessageBox.Show("handler event");
            XmlDocument doc = new XmlDocument();
            string read_string = "";
            doc.LoadXml(args.XmlResult);

            foreach (XmlNode node2 in doc.DocumentElement.ChildNodes)
            {
                if (node2.Name.Equals("general"))
                {
                    foreach (XmlNode node in node2.ChildNodes)
                    {
                        if (node.Name.Equals("full_string"))
                        {
                            read_string = node.InnerText;

                            foreach (XmlAttribute att in node.Attributes)
                            {
                                if (att.Name.Equals("encoding") && att.InnerText.Equals("base64") && !String.IsNullOrEmpty(node.InnerText))
                                {
                                    byte[] code = Convert.FromBase64String(node.InnerText);
                                    read_string = System.Text.Encoding.Default.GetString(code, 0, code.Length);
                                    IDReaderCommFunctions.ScannedString = read_string;
                                    AddListItem("XML string arrived : resultId = " + args.ResultId + ", read string = " + read_string);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void OnImageGraphicsArrived(object sender, ImageGraphicsArrivedEventArgs args)
        {
             //If the current result info has the same result ID as the one we've just
             //received, store the image graphics in it, otherwise create a new result info object.
            //lock (_currentResultInfoSyncLock)
            //{
            //    if (_currentResultInfo.ResultId == args.ResultId)
            //        _currentResultInfo.ImageGraphics = args.ImageGraphics;
            //    else
            //        _currentResultInfo = new IDReader.IDReaderCommFunctions.ResultInfo(args.ResultId, null, args.ImageGraphics, null);
            //}

            //_syncContext.Post(
            //    delegate
            //    {
            //        AddListItem("Image graphics arrived (rid=" + args.ResultId.ToString() + ")");
            //        ShowResult();
            //    },
            //    null);
        }

        private void OnImageArrived(object sender, ImageArrivedEventArgs args)
        {
            // If the current result info has the same result ID as the one we've just
            // received, store the image in it, otherwise create a new result info object.
            lock (_currentResultInfoSyncLock)
            {
                //if (_currentResultInfo.ResultId == args.ResultId)
                //{
                //    _currentResultInfo.Image = null;
                //    _currentResultInfo.Image = args.Image;
                //    System.Windows.Forms.MessageBox.Show("Old Image");
                    
                //}
                //else
                //{
                    _currentResultInfo = null;
                    _currentResultInfo = new IDReader.IDReaderCommFunctions.ResultInfo(args.ResultId, args.Image, null, null);
                    //System.Windows.Forms.MessageBox.Show("New Image");
                //}
            }

            _syncContext.Post(
                delegate
                {
                    AddListItem("Image arrived (rid=" + args.ResultId.ToString() + "), size = " + args.Image.Width.ToString() + "x" + args.Image.Height.ToString());
                    ShowResult();
                },
                null);
        }

        //public static ImageSource BitmapFromUri(Uri source)
        //{
        //    var bitmap = new BitmapImage();
        //    bitmap.BeginInit();
        //    bitmap.UriSource = source;
        //    bitmap.CacheOption = BitmapCacheOption.OnLoad;
        //    bitmap.EndInit();
        //    return bitmap;
        //}

        public static ImageSource BitmapFromUri(Uri source)
        {
            var bitmap = new BitmapImage();

            bitmap.BeginInit();
            bitmap.UriSource = null;
            bitmap.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            bitmap.UriSource = source;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }

        private void ShowResult()
        {
            System.Drawing.Image image = null;
            string image_graphics = null;
            string read_result = null;
            //picResultImage.Source = null;
            // Take a reference or copy values from the locked result info object. This is done
            // so that the lock is used only for a short period of time.
            lock (_currentResultInfoSyncLock)
            {
                image = _currentResultInfo.Image;
                image_graphics = null;// _currentResultInfo.ImageGraphics;
                read_result = _currentResultInfo.ReadString;
            }
            if (image != null)
            {
                //lbReadString.Dispatcher.BeginInvoke(new Action(() => lbReadString.IsEnabled = false));
                try
                {
                    image.Save("D:\\Image\\image1.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    picResultImage.Dispatcher.BeginInvoke(new Action(() => picResultImage.Source = BitmapFromUri(new Uri(@"D:\Image\image1.jpeg")))); //new BitmapImage(new Uri("D:\\Image\\image1.jpeg"))));
                }
                catch (Exception e)
                {
                    //System.Windows.Forms.MessageBox.Show(e.Source);
                }
            }

            if (IDReaderCommFunctions.ScannedString.Length != 0 )
            {
                lbReadString.Dispatcher.BeginInvoke(new Action(() => lbReadString.Content = IDReaderCommFunctions.ScannedString));
            }

        }

        private void btnRead_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearScannedValues();                
                AddListItem("Trigger ON");
                _system.SendCommand("TRIGGER ON");
                Thread.Sleep(1000);  
                AddListItem("Trigger OFF");
                _system.SendCommand("TRIGGER OFF");
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Failed to send TRIGGER  commands: " + ex.ToString());
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(0);
        }
    
        /******************************SCANNER CYCLE BUTTONS*************************************/
  
        private void btnScannerStartStop_Click(object sender, RoutedEventArgs e)
        {
            
                Thread scanThread = new Thread(CellROCommFunctions.ScannerCycle);
                System.Windows.Controls.Button startStop = (System.Windows.Controls.Button)sender;
                bool manualCnxn = false;

                if (startStop.Name == "btnScannerStart")
                {
                    if (IDReaderCommFunctions.ScannerIPAddress.Length != 0)
                    {
                        if (btnIDRDisconnect.IsEnabled)
                        {
                                manualCnxn = false;
                                btnIDRDisconnect.IsEnabled = false;
                                btnTrigger.IsEnabled = false;
                        }
                        if (!manualCnxn)
                        {
                            if (ScannerConnect())
                            {
                                IDReaderCommFunctions.ResetScannerSignals();
                                scanThread.Start();
                                IDReaderCommFunctions.ScannerStarted = true;
                                btnScannerStart.IsEnabled = false;
                                btnScannerStop.IsEnabled = true;
                                btnScannerPause.IsEnabled = true;
                                btnScannerResume.IsEnabled = false;
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("Failed to connect to scanner, check connection", "Alarm");
                            }
                        }
                    }
                    else
                        System.Windows.Forms.MessageBox.Show("Please select the device using settings", "Info");
                }
                if (startStop.Name == "btnScannerStop")
                {
                    if (!IDReaderCommFunctions.ScannerBusyStatus)
                    {
                        if (System.Windows.Forms.MessageBox.Show("Are you sure you want to stop the ScannerCycle?", "Warning", MessageBoxButtons.YesNo) == (DialogResult)MessageBoxResult.Yes)
                        {
                            //scanThread.Abort();
                            IDReaderCommFunctions.ScannerStarted = false;
                            IDReaderCommFunctions.ResetScannerSignals();
                            //scanThread = null;
                            btnScannerStop.IsEnabled = false;
                            btnScannerStart.IsEnabled = true;
                            btnScannerPause.IsEnabled = false;
                            btnScannerResume.IsEnabled = false;
                            if (IDReaderCommFunctions.ScannerConnected == true)
                            {
                                btnIDRDisconnect.IsEnabled = true;
                                btnTrigger.IsEnabled = true;
                            }
                            else
                            {
                                btnIDRConnect.IsEnabled = true;
                            }                       
                        }
                       }
                    else
                        System.Windows.Forms.MessageBox.Show("Scanner is busy.Please try after some time.", "Warning");
                }
            
        }

        private void btnScannerReset_Click(object sender, RoutedEventArgs e)
        {
            if ((!RobotSignals.ReqScanFirstTime && !RobotSignals.ReqScanPrinted) || !IDReaderCommFunctions.ScannerStarted)
            {
                IDReaderCommFunctions.ResetScannerSignals();
                txtIDRDeviceIP.Text = IDReaderCommFunctions.ScannerIPAddress;
                txtIDRDeviceName.Text = IDReaderCommFunctions.ScannerDeviceID;
            }
            else
                System.Windows.Forms.MessageBox.Show("Scanner cycle has to be stopped and reset the ReqScanFirstTime/ReqScanPrinted if HIGH.", "Warning");
        }

        private void InitializeScannerCycle()
        {
            //IDReaderCommFunctions.ResetScannerSignals();
            //btnScannerPause.IsEnabled = false;
            //btnScannerStart.IsEnabled = true;
            //btnScannerResume.IsEnabled = false;
            //btnScannerStop.IsEnabled = false;
            //txtIDRDeviceName.Text = "";
            //txtIDRDeviceIP.Text = "";
            //btnIDRConnect.IsEnabled = true;
            //btnIDRDisconnect.IsEnabled = false;
            //btnTrigger.IsEnabled = false;
            ////btnClear.IsEnabled = false;

            Thread scanThread = new Thread(CellROCommFunctions.ScannerCycle);

            IDReaderCommFunctions.ResetScannerSignals();
            ScannerConnect();
            scanThread.Start();
            IDReaderCommFunctions.ScannerStarted = true;
            btnScannerStart.IsEnabled = false;
            btnScannerStop.IsEnabled = true;
            btnScannerPause.IsEnabled = true;
            btnScannerResume.IsEnabled = false;
            btnIDRConnect.IsEnabled = false;
            btnIDRDisconnect.IsEnabled = false;
            btnTrigger.IsEnabled = false;
            txtIDRDeviceName.Text = IDReaderCommFunctions.ScannerDeviceID;
            txtIDRDeviceIP.Text = IDReaderCommFunctions.ScannerIPAddress;
        }

        private void btnScannerPauseResume_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button pauseResume = (System.Windows.Controls.Button)sender;
            if (pauseResume.Name == "btnScannerPause")
            {
                if (!IDReaderCommFunctions.ScannerBusyStatus)
                {
                    IDReaderCommFunctions.ScannerPaused = true;
                    btnScannerPause.IsEnabled = false;
                    btnScannerResume.IsEnabled = true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Scanner is busy.Please try after some time", "Warning");
                }
            }
            if (pauseResume.Name == "btnScannerResume")
            {
                IDReaderCommFunctions.ScannerPaused = false;
                btnScannerPause.IsEnabled = true;
                btnScannerResume.IsEnabled = false;
            }
        }

        private void btnScannerSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!IDReaderCommFunctions.ScannerStarted)
            {
                // Open a dialog window to edit the scanner settings
                ScannerSettings _ScannerSettings = new ScannerSettings();
                if (!_ScannerSettings.IsVisible)
                {
                     _ScannerSettings.ShowDialog();
                    _ScannerSettings.Activate();
                }
                else{
                    
                    _ScannerSettings.Close();
                    _ScannerSettings = null;
                }
                //System.Windows.Forms.MessageBox.Show(IDReaderCommFunctions.ScannerIPAddress);
                txtIDRDeviceName.Text = IDReaderCommFunctions.ScannerDeviceID;
                txtIDRDeviceIP.Text = IDReaderCommFunctions.ScannerIPAddress;
            }
            else
                System.Windows.Forms.MessageBox.Show("Scannercycle has to be stopped, to edit the settings.", "Warning");           
        }

        private void ClearScannedValues()
        {
            picResultImage.Source = null;        
            lbReadString.Content = "";
            listBox1.Items.Clear();
            if (System.IO.File.Exists(@"D:\Image\image1.jpeg")) System.IO.File.Delete(@"D:\Image\image1.jpeg");
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearScannedValues();
        }

#endregion

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(nameOrAddress);

                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }

            return pingable;
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RobotCommFunctions.RobotStarted = false;
            AutomatorCommFunctions.PrinterStarted = false;
            IDReaderCommFunctions.ScannerStarted = false;

            base.OnClosed(e);

            App.Current.Shutdown();
        }

        private void DB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

    }
}
