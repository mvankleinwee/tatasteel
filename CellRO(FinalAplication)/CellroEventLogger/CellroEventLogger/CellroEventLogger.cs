﻿using System;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace CellroEventLogger
{
    public class EventLogger
    {
        static void Main(string[] args)
        {

            //EventLog("Enter your Log Message...");
            //System.Console.Read();

        }

        public static void EventLog(string Msg)
        {
            string LogFolder;
            string LogFile;
            //string Msg;
            string datePatt;

            LogFolder = @"D:\TataSteelLogs";  // @"C:\TataSteelLogs\Sample
            datePatt = @"d_M_yyyy";
            LogFile = "CellroEventLogs_" + DateTime.Now.ToString(datePatt) + ".txt";
            //Msg = "Logging my Error";

            if (Directory.Exists(LogFolder))
            {
                //Do Nothing
            }
            else
            {
                Directory.CreateDirectory(LogFolder);
            }

            LogFile = Path.Combine(LogFolder, LogFile);
            using (StreamWriter File = new StreamWriter(LogFile, true))
            {
                File.WriteLine(Msg);
            }
        }
    }
}
