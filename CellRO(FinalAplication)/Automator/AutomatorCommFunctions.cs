﻿using System;
using System.IO.Ports;
using System.Data.SqlClient;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using CellroEventLogger;

namespace Automator
{
    public static class AutomatorCommFunctions
    {
        public static bool LastDotMarked { get; set; }
        public static bool PrinterErrorStatus { get; set; }
        public static SerialPort printerPort { get; set; }
        public static bool PrinterPaused { get; set; }
        public static bool PrinterStarted { get; set; }
        public static bool PrinterBusyStatus { get; set; }
        public static string PortName { get; set; }
        public static int BaudRate { get; set; }
        public static int DataBits { get; set; }
        public static string StringAtReceivedBuffer { get; set; }
        public static bool PrinterPortOpen = false;

        public static void ResetPrinterSignals()
        {
            PrinterErrorStatus = false;
            PrinterPaused = false;
            PrinterBusyStatus = false;
            LastDotMarked = false;
            PortName = "COM4";
            BaudRate = 9600;
            DataBits = 8;
            StringAtReceivedBuffer = string.Empty;
        }


        public static bool PrinterConnect(string portName, int baudRate, int dataBits)
        {
            if (printerPort != null) {return printerPort.IsOpen; }
            else
            {
                printerPort = new SerialPort(portName, baudRate, Parity.None, dataBits, StopBits.One);
                try
                {
                    if (printerPort.IsOpen) printerPort.Close();
                    printerPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                    printerPort.Open();
                    PrinterPortOpen = true;
                    return true;
                }

                catch { if (printerPort != null) printerPort = null; }
            }
            return false;

        }

        public static bool PrinterStillConnected()
        {
            if (printerPort != null) { return printerPort.IsOpen; }
            else
                return false;
        }

        public static void PrinterDisConnect()
        {
           if (printerPort != null)
           { 
               if (printerPort.IsOpen)
               {
                   printerPort.Close();                   
               }
           }
           PrinterPortOpen = false;
           printerPort = null;         
        }

        public static void PrintMessage(string message)
        {
            // opens the Serial port and writes the message on the printer buffer.
            LastDotMarked = false;
            PrinterErrorStatus = false;
            if (PrinterConnect(PortName, BaudRate, DataBits))
            {
                printerPort.Write(message);
            }
            else
            {
                MessageBox.Show("Printer not connected");
                if (printerPort != null) printerPort = null;
            }
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            StringAtReceivedBuffer = "";
            string indata = sp.ReadExisting();
            StringAtReceivedBuffer = indata;
            //if (indata.IndexOf(((char)5).ToString()) > -1) Console.WriteLine("ENQ"); // Back to Home Postion
            if (indata.IndexOf(((char)4).ToString()) > -1) LastDotMarked = true; //Last Dot Marked
            if (LastDotMarked)
            {
                if (indata.IndexOf(((char)15).ToString()) > -1) PrinterErrorStatus = true;  //Error if any
                //if (sp.IsOpen) sp.Close();
            }
            if (indata.IndexOf(((char)15).ToString()) > -1) { CellroEventLogger.EventLogger.EventLog("Time Out Error - " + indata); PrinterErrorStatus = true; }// PrinterErrorStatus = true;
            if (indata.IndexOf(((char)07).ToString()) > -1) { CellroEventLogger.EventLogger.EventLog("[BEL] : missing file - " + indata); PrinterErrorStatus = true; } //PrinterErrorStatus = true;
            if (indata.IndexOf(((char)08).ToString()) > -1) { CellroEventLogger.EventLogger.EventLog("[BS] : CheckSum error - " + indata); PrinterErrorStatus = true; } //PrinterErrorStatus = true;
            if (indata.IndexOf(((char)09).ToString()) > -1) { CellroEventLogger.EventLogger.EventLog("[HT] : Format error - " + indata); PrinterErrorStatus = true; } // PrinterErrorStatus = true;
            //if (indata.IndexOf(((char)0X0A).ToString()) > -1) { CellroEventLogger.EventLogger.EventLog("[LF] : Missing Variable - " + indata); PrinterErrorStatus = true; }// PrinterErrorStatus = true;
        }
    }
}