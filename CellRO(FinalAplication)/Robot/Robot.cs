﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FRRJIf;
using System.Diagnostics;

namespace Robot
{
    public class RobotSignals
    {
        public static bool ReqAlive { get; set; }
        public static bool ReqPrint { get; set; }
        public static bool ReqScanFirstTime { get; set; }
        public static bool ReqScanPrinted { get; set; }
        public static bool ReqDeleteItem { get; set; }
        public static bool ReqDeleteAll { get; set; }
        public static bool ReqReadMeasuredValue { get; set; }       //20180504 MvK: Added for logging measured values
        public static bool AckAlive { get; set; }
        public static bool AckPrintOK { get; set; }
        public static bool AckPrintNOK { get; set; }
        public static bool AckScanFirstTimeOK { get; set; }
        public static bool AckScanFirstTimeNOK { get; set; }
        public static bool AckScanPrintedOK { get; set; }
        public static bool AckScanPrintedNOK { get; set; }
        public static bool AckDeleteItemOK { get; set; }
        public static bool AckDeleteAllOK { get; set; }
        public static bool AckReadMeasuredValue { get; set; }       //20180504 MvK: Added for logging measured values
        public static int ReqPrint_ItemID { get; set; }
        public static int ReqDeleteItem_ItemID { get; set; }
        public static int ReqScanPrinted_ItemID { get; set; }
        public static int AckScanFirstTimeOK_ItemID { get; set; }
        public static double ReqReadMeasuredValue_Value { get; set; }  //20180504 MvK: Added for logging measured values
        public static int LastScanned_ItemID { get; set; }          //20180504 MvK: Added for logging measured values

        //20180509 MvK: Keep latest scanned data for logging purpose
        public static int ItemID { get; set; }
        public static double AskedThickness { get; set; }
        public static double TolUnder { get; set; }
        public static double TolAbove { get; set; }
    }

    public class RobotCommFunctions
    {
        public static bool RobotPaused { get; set; }
        public static bool RobotStarted { get; set; }
        public static bool RobotBusyStatus { get; set; }
        public static string RobotIPAddress { get; set; }
        public static Core mobjCore;
        public static DataTable mobjDataTable;
        public static DataNumReg mobjNumReg;
        public static bool RoConnected;

        public static void ResetRobotSignals()
        {
            RobotSignals.ReqAlive = false;
            RobotSignals.ReqPrint = false;
            RobotSignals.ReqScanFirstTime = false;
            RobotSignals.ReqScanPrinted = false;
            RobotSignals.ReqDeleteItem = false;
            RobotSignals.ReqDeleteAll = false;
            RobotSignals.ReqReadMeasuredValue = false;      //20180504 MvK: Added for logging measured values
            RobotSignals.AckPrintOK = false;
            RobotSignals.AckPrintNOK = false;
            RobotSignals.AckScanFirstTimeOK = false;
            RobotSignals.AckScanFirstTimeNOK = false;
            RobotSignals.AckScanPrintedOK = false;
            RobotSignals.AckScanPrintedNOK = false;
            RobotSignals.AckDeleteItemOK = false;
            RobotSignals.AckDeleteAllOK = false;
            RobotSignals.AckReadMeasuredValue = false;      //20180504 MvK: Added for logging measured values
            RobotSignals.ReqPrint_ItemID = 0;
            RobotSignals.ReqDeleteItem_ItemID = 0;
            RobotSignals.ReqScanPrinted_ItemID = 0;
            RobotSignals.AckScanFirstTimeOK_ItemID = 0;
            RobotSignals.ReqReadMeasuredValue_Value = 0;    //20180504 MvK: Added for logging measured values
            RobotPaused = false;
            RobotBusyStatus = false;
            RobotIPAddress = "192.168.200.3";       //"192.168.200.3";

        }

        public static void RobotDisconnected()
        {
            if (mobjCore != null)
            {
                mobjCore.Disconnect();
            }
            RoConnected = false;
            mobjCore = null;
            mobjDataTable = null;
            mobjNumReg = null;

        }

        public static bool GetRequests()
        {
            Array intSDO1 = new short[10];
            object VntValue = null;
            object VntValue2 = null;         //20180504 MvK: Read extra Register fro mrobot (Measured value)
            int Temp_Measured;
            double Measured;
            try
            {
                if (mobjDataTable.Refresh() == false)
                {
                    return false;
                }
                if (mobjCore.ReadSDO(100, ref intSDO1, 10) == false) //first value is starting DO  //20180504 MvK: my guess is to change 9 to 10 (10 Outputs to read)
                {
                    return false;
                }

                if (mobjNumReg.GetValue(600, ref VntValue) == false)
                {
                    RobotSignals.AckScanFirstTimeOK_ItemID = 0;
                    return false;
                }

                if (mobjNumReg.GetValue(599, ref VntValue2) == false)       //20180504 MvK: Read extra Register from robot (Measured value)
                {
                    RobotSignals.ReqReadMeasuredValue_Value = 0;
                    return false;
                }
                else
                {
                    //20180517 MvK: Didn't know how to read Double values, Didn't work somehow.
                    Temp_Measured = (int)VntValue2;
                    Measured = (double)Temp_Measured / 100000;
                }

                for (int i = 0; i < 10; i++)        
                {
                    if ((short)intSDO1.GetValue(i) == 0)
                    {
                        if (i == 0) { RobotSignals.ReqAlive = false; }
                        if (i == 1) { RobotSignals.ReqScanFirstTime = false; }
                        if (i == 3) { RobotSignals.ReqPrint = false; RobotSignals.ReqPrint_ItemID = 0; }  //Added by Joe: We need to reset ItemsID, dont we?
                        if (i == 5) { RobotSignals.ReqScanPrinted = false; RobotSignals.ReqScanPrinted_ItemID = 0; }
                        if (i == 7) { RobotSignals.ReqDeleteAll = false; }
                        if (i == 8) { RobotSignals.ReqDeleteItem = false; RobotSignals.ReqDeleteItem_ItemID = 0; }
                        if (i == 9) { RobotSignals.ReqReadMeasuredValue = false; RobotSignals.ReqReadMeasuredValue_Value = 0; }          //20180504 MvK: Set new variable to false
                    }
                    else
                    {
                        if (i == 0) { RobotSignals.ReqAlive = true; }
                        if (i == 1) { RobotSignals.ReqScanFirstTime = true; }
                        if (i == 3) { RobotSignals.ReqPrint = true; RobotSignals.ReqPrint_ItemID = (int)VntValue; }
                        if (i == 5) { RobotSignals.ReqScanPrinted = true; RobotSignals.ReqScanPrinted_ItemID = (int)VntValue; }
                        if (i == 7) { RobotSignals.ReqDeleteAll = true; }
                        if (i == 8) { RobotSignals.ReqDeleteItem = true; RobotSignals.ReqDeleteItem_ItemID = (int)VntValue; }
                        if (i == 9) { RobotSignals.ReqReadMeasuredValue = true; RobotSignals.ReqReadMeasuredValue_Value = Measured; }           //20180504 MvK: Set new variable to True
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool WriteAcknowledgements(int[] Values)      //, int Values2
        {
            Array intSDO2 = new short[10];

            try
            {
                if (RobotSignals.AckScanFirstTimeOK == true)
                {
                    //for (int i = 0; i < 14; i++)
                    //{
                    //    if (mobjNumReg.SetValue((601 + i), Values[i]) == false)
                    //    {
                    //        WriteToEventLog("NumReg SetValues failed, for i = " + i);
                    //        if (i == 14) { return false; };
                    //    }
                    //}
                    if (mobjNumReg.SetValues(601, Values, 14) == false)  //firstvalue is first registernumber
                    {
                        WriteToEventLog("NumReg SetValues failed");
                        return false;
                    }

                    //if (mobjNumReg.SetValue(601, Values2) == false)  //firstvalue is first registernumber   
                    //{
                    //    WriteToEventLog("NumReg SetValues2 failed");
                    //    //return false;
                    //}
                }

                if (RobotSignals.AckAlive == true) { intSDO2.SetValue((short)1, 0); }
                else { intSDO2.SetValue((short)0, 0); }
                if (RobotSignals.AckScanFirstTimeOK == true) { intSDO2.SetValue((short)1, 1); }
                else { intSDO2.SetValue((short)0, 1); }
                if (RobotSignals.AckScanFirstTimeNOK == true) { intSDO2.SetValue((short)1, 2); }
                else { intSDO2.SetValue((short)0, 2); }
                if (RobotSignals.AckPrintOK == true) { intSDO2.SetValue((short)1, 3); }
                else { intSDO2.SetValue((short)0, 3); }
                if (RobotSignals.AckPrintNOK == true) { intSDO2.SetValue((short)1, 4); }
                else { intSDO2.SetValue((short)0, 4); }
                if (RobotSignals.AckScanPrintedOK == true) { intSDO2.SetValue((short)1, 5); }
                else { intSDO2.SetValue((short)0, 5); }
                if (RobotSignals.AckScanPrintedNOK == true) { intSDO2.SetValue((short)1, 6); }
                else { intSDO2.SetValue((short)0, 6); }
                if (RobotSignals.AckDeleteAllOK == true) { intSDO2.SetValue((short)1, 7); }
                else { intSDO2.SetValue((short)0, 7); }
                if (RobotSignals.AckDeleteItemOK == true) { intSDO2.SetValue((short)1, 8); }
                else { intSDO2.SetValue((short)0, 8); }
                if (RobotSignals.AckReadMeasuredValue == true) { intSDO2.SetValue((short)1, 9); }
                else { intSDO2.SetValue((short)0, 9); }

                if (mobjCore.WriteSDO(110, ref intSDO2, 10) == false) //firstvalue is starting DO       //20180517 MvK: 3rd argument changed from 9 to 10
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                WriteToEventLog("Thrown exception RobotCommunication: " + ex);
                return false;
            }

            return true;
        }

        public static bool RobotConnect(string RobotIPAdress)
        {

            if (mobjCore == null)
            {
                try
                {
                    mobjCore = new Core();
                    mobjDataTable = mobjCore.get_DataTable();
                    {
                        mobjNumReg = mobjDataTable.AddNumReg(FRRJIf.FRIF_DATA_TYPE.NUMREG_INT, 599, 619);   // second and third argument is start/end index      20180507 MvK: 599 to 619, 599 added for reading measure data.
                    }
                   
                    if (mobjCore.Connect(RobotIPAdress) == true)
                    {
                        RoConnected = true;
                        return true;
                    }
                    else
                    {
                        mobjCore = null;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    WriteToEventLog("Thrown exception RobotCommunication (robotconnect): " + ex);
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        private static void WriteToEventLog(string message)
        {
            string cs = "Cellro";
            EventLog elog = new EventLog();
            if (!EventLog.SourceExists(cs))
            {
                EventLog.CreateEventSource(cs, cs);
            }
            elog.Source = cs;
            elog.EnableRaisingEvents = true;
            elog.WriteEntry(message);
        }
    }
}
