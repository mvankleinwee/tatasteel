﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//using System.Windows.Controls;
using System.Drawing;
using System.Net;
using Cognex.DataMan.Discovery;


namespace IDReader
{
    public class IDReaderCommFunctions
    {
        public static bool ScannerPaused { get; set; }
        public static bool ScannerStarted { get; set; }
        public static bool ScannerBusyStatus { get; set; }
        public static bool ScannerConnected { get; set; }
        public static string ScannedString { get; set; }
        public static string ScannerIPAddress { get; set; }
        public static string ScannerDeviceID { get; set; }
        public static string ScannerUserID { get; set; }
        public static string ScannerPwd {get; set; }
        public static string ScannerTriggered { get; set; }

        public static void ResetScannerSignals()
        {
            ScannerPaused = false;
            ScannerBusyStatus = false;
            ScannerTriggered = "OFF";
            ScannedString = "";
            ScannerDeviceID = "DM200_176664";
            ScannerIPAddress = "192.168.200.6";
            ScannerPwd = "";
            ScannerUserID = "Admin";
        }

        public class ResultInfo
        {
            public ResultInfo(int resultId, System.Drawing.Image image, string imageGraphics, string readString)
            {
                ResultId = resultId;
                Image = image;
                ImageGraphics = imageGraphics;
                ReadString = readString;
            }

            public int ResultId { get; set; }
            public System.Drawing.Image Image { get; set; }
            public string ImageGraphics { get; set; }
            public string ReadString { get; set; }
        }
               
    }
}
