<%@ Import namespace="Castle.Core"%>
<%@ Import Namespace="Castle.Facilities.WcfIntegration" %>
<%@ Import Namespace="Castle.MicroKernel.Registration" %>
<%@ Import Namespace="IJM.KRN.Klabbes.Trekbank.Contracts.ServiceContracts" %>
<%@ Import Namespace="IJM.KRN.Klabbes.Trekbank.ServiceImplementation" %>
<%@ Import Namespace="IJM.Library.Logging" %>
<%@ Import namespace="log4net.Config"%>
<%@ Import namespace="IJM.Library"%>
<%@ Import namespace="IJM.Library.Threading"%>
<%@ Import Namespace="IJM.Library.IoC" %>
<%@ Import namespace="IJM.Library.Security"%>
<%@ Import namespace="IJM.KRN.Klabbes.Trekbank.BusinessClasses"%>
<%@ Import Namespace="IJM.KRN.Klabbes.Trekbank.BusinessLogic" %>
<%@ Import namespace="IJM.KRN.Klabbes.Trekbank.BusinessClasses.Repositories"%>
<%@ Import Namespace="IJM.KRN.Klabbes.Trekbank.DataAccess" %>

<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

        //Add Repositories to Container
		ServiceContainer.Current.AddComponent("KeurplaatRepository", typeof(IKeurplaatRepository), typeof(KeurplaatRepository));
		ServiceContainer.Current.AddComponent("StandaardStaafRepository", typeof(IStandaardStaafRepository), typeof(StandaardStaafRepository));
        ServiceContainer.Current.AddComponent("KerfslagProevenRepository", typeof(IKerfslagProevenRepository), typeof(KerfslagProevenRepository));
		ServiceContainer.Current.AddComponent("ViewInstronTrekbankGetRepository", typeof(IViewInstronTrekbankGetRepository), typeof(ViewInstronTrekbankGetRepository));
		ServiceContainer.Current.AddComponent("ControleStaafInstellingRepository", typeof(IControleStaafInstellingRepository), typeof(ControleStaafInstellingRepository));
		ServiceContainer.Current.AddComponent("ControleStaafResultaatRepository", typeof(IControleStaafRepository), typeof(ControleStaafResultaatRepository));
		ServiceContainer.Current.AddComponent("ProefInstallatieRepository", typeof(IProefInstallatieRepository), typeof(ProefInstallatieRepository));
        ServiceContainer.Current.AddComponent("TrekproefInstelGegevensRepository", typeof(ITrekproefInstelGegevensRepository), typeof(TrekproefInstelGegevensRepository));
        ServiceContainer.Current.AddComponent("SysteemTrekproefValidatieRepository", typeof(ISysteemTrekproefValidatieRepository), typeof(SysteemTrekproefValidatieRepository));
        ServiceContainer.Current.AddComponent("SysteemKerfslagTolerantieRepository", typeof(ISysteemKerfslagTolerantieRepository), typeof(SysteemKerfslagTolerantieRepository));

        ServiceContainer.CurrentContainer.AddFacility<WcfFacility>();
        
        ServiceContainer.Current.Register(Component.For<IKerfslagBewerkingService>()
            .ImplementedBy<KerfslagBewerkingService>()
            .AsWcfService(new DefaultServiceModel().Hosted()));
        
        
        //Add businessProcesses to Container
        ServiceContainer.Current.AddComponent("TrekbankBusinessProcess", typeof(TrekbankBusinessProcess));
        ServiceContainer.Current.AddComponent("KerfslagBewerkingBusinessProcess", typeof(KerfslagBewerkingBusinessProcess));
              
        //Add ImpersonateUser to ServiceContainer
        ServiceContainer.Current.AddComponent("ImpersonateUser", typeof (IImpersonateUser), typeof (ImpersonateUser));
        
        //Add Taskrunner to container, deze taskrunner start een taak in een nieuwe thread
        ServiceContainer.Current.AddComponent("TaskRunner", typeof(ITaskRunner), typeof(ConcurrentTaskRunner)); 
        
        //Add Globalenvironment nodig voor de Ijm.Library aan de container
//        ServiceContainer.Current.AddComponent("Environment", typeof(IGlobalEnvironment), typeof(GlobalEnvironment));
//        //ServiceContainer.Current.AddComponent("IEnvironment", typeof(DV23.Library.Mbs.IEnvironment), typeof(DV23.Library.Mbs.Environment));

        // Enable NHibernate logging
        XmlConfigurator.Configure();
    }

    //void Application_End(object sender, EventArgs e)
    //{
    //    //  Code that runs on application shutdown

    //}

    //void Application_Error(object sender, EventArgs e)
    //{
    //    // Code that runs when an unhandled error occurs

    //}

    //void Session_Start(object sender, EventArgs e)
    //{
    //    // Code that runs when a new session is started

    //}

    //void Session_End(object sender, EventArgs e)
    //{
    //    // Code that runs when a session ends. 
    //    // Note: The Session_End event is raised only when the sessionstate mode
    //    // is set to InProc in the Web.config file. If session mode is set to StateServer 
    //    // or SQLServer, the event is not raised.

    //}
       
</script>

