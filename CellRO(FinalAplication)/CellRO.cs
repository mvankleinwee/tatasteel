﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Data.Entity;
using Automator;
using System.Data.Objects;
using System.Data.EntityClient;
using System.Threading;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using System.Data;
using IDReader;
using Robot;
using CellRO.ServiceReference_Tata1;
using CellroEventLogger;
using System.Timers;

namespace CellRO
{
    public class CellROCommFunctions
    {

        static System.Timers.Timer timer1 = new System.Timers.Timer(35000); 
        static bool RetryReqPrint = false;
        static bool FirstTime = false;

        
        static void timer_Tick(object sender, System.EventArgs e)
        {
            //if ReqPrint = 1 and Ack are both 0 Then retry for printer
            if (RobotSignals.ReqPrint && !RobotSignals.AckPrintOK && !RobotSignals.AckPrintNOK)
            {
                RetryReqPrint = true;
                EventLogger.EventLog("RetryReqPrint is Set!!! (Printing doesn't print?)");
            }
            else
            { 
                EventLogger.EventLog("Printing should be succesfull!!!"); 
            }
                timer1.Stop();
        }
        
        private static void WriteToEventLog(string message)
        {
            string cs = "Cellro";
            EventLog elog = new EventLog();
            if (!EventLog.SourceExists(cs))
            {
                EventLog.CreateEventSource(cs, cs);
            }
            elog.Source = cs;
            elog.EnableRaisingEvents = true;
            elog.WriteEntry(message);
        }

        public static void ReqAlive()
        {
            if (RobotSignals.ReqAlive == true) { RobotSignals.AckAlive = true; }
            else { RobotSignals.AckAlive = false; }
        }

        public static void ScannerCycle()
        {
            try
            {

                //Insert into SampleData data from scanner.
                //DBContextDataContext db = new DBContextDataContext();   // CellRODB Database connection
                //TataWCFService.TataWCFServiceClient myService = new TataWCFService.TataWCFServiceClient();  //WCF connection
                //List<TataKLABBESData> recTata = new List<TataKLABBESData>();  //Based on Tata table

                KerfslagBewerkingServiceClient _KerfslagBewerkingServiceClient = new KerfslagBewerkingServiceClient();
                KerfslagBewerkingAanvraagBericht _KerfslagBewerkingAanvraagBericht = new KerfslagBewerkingAanvraagBericht();
                KerfslagBewerkingAanvraagBerichtHeader_Geg _KerfslagBewerkingHeader = new KerfslagBewerkingAanvraagBerichtHeader_Geg();
                KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag _KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag = new KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag();
                KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat = new KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat();
                KerfslagBewerkingResultaatBericht _KerfslagBewerkingResultaatBericht = new KerfslagBewerkingResultaatBericht();

                SampleData sd = new SampleData();   //Local DB at CellRO side.
                bool exitCondition = false;
                bool busyScanFirstTimeFlag = false;
                bool busyScanPrintFlag = false;
                int Werkplaatsnummer = 0;
                int ProefstukUitnameCode = 0;
                int ProefIndex = 0;

                while (IDReaderCommFunctions.ScannerStarted)
                {
                    exitCondition = IDReaderCommFunctions.ScannerPaused;
                    if (!exitCondition)
                    {
                        IDReaderCommFunctions.ScannerBusyStatus = busyScanFirstTimeFlag || busyScanPrintFlag;

                        /************************************************************************************************************************/
                        //********Step1. Check if ReqScanPrint bit is High and resp. Ack Bits are low.*****************
                        /************************************************************************************************************************/
                        DBContextDataContext db = new DBContextDataContext();   // CellRODB Database connection

                        int ItemID = 0;
                        bool _ReqScanFirstTime = RobotSignals.ReqScanFirstTime;
                        bool _AckScanFirstTimeOK = RobotSignals.AckScanFirstTimeOK;
                        bool _AckScanFirstTimeNOK = RobotSignals.AckScanFirstTimeNOK;
                        bool _ReqScanPrinted = RobotSignals.ReqScanPrinted;
                        bool _AckScanPrintedOK = RobotSignals.AckScanPrintedOK;
                        bool _AckScanPrintedNOK = RobotSignals.AckScanPrintedNOK;
                        int _ReqScanPrinted_ItemID = RobotSignals.ReqScanPrinted_ItemID;
                        bool scanOKFlag = true;
                        bool reqScanFirstTime = false;
                        bool reqScanPrinted = false;
                        string messageFromScanner = string.Empty;
                        string messageFromDB = string.Empty;

                        if (!reqScanPrinted && !busyScanPrintFlag)
                        {
                            //Checking if Req Bit is  high,if AckOK Bit is  low,if AckNOK Bit is  low
                            if (_ReqScanFirstTime && !_AckScanFirstTimeOK && !_AckScanFirstTimeNOK)
                            { if (!(bool)busyScanFirstTimeFlag) reqScanFirstTime = true; }
                            else
                            {
                                if (!_ReqScanFirstTime && (_AckScanFirstTimeOK || _AckScanFirstTimeNOK))  // if Req is LOW, then Make the Ack bit to LOW
                                {
                                    RobotSignals.AckScanFirstTimeOK = false;
                                    RobotSignals.AckScanFirstTimeNOK = false;
                                    busyScanFirstTimeFlag = false;
                                    ////May be need to nullify the ItemID in the CnxnReqData along AckScanReadyOK
                                    RobotSignals.AckScanFirstTimeOK_ItemID = 0;
                                }
                            }
                        }

                        if (!reqScanFirstTime && !busyScanFirstTimeFlag)
                        {
                            if (_ReqScanPrinted && !_AckScanPrintedOK && !_AckScanPrintedNOK && _ReqScanPrinted_ItemID != 0)
                            { if (!(bool)busyScanPrintFlag) reqScanPrinted = true; }
                            else
                            {
                                if (!_ReqScanPrinted && (_AckScanPrintedOK || _AckScanPrintedNOK))  // if Req is LOW, then Make the Ack bit to LOW
                                {
                                    RobotSignals.AckScanPrintedOK = false;
                                    RobotSignals.AckScanPrintedNOK = false;
                                    busyScanPrintFlag = false;
                                }
                            }
                        }

                        /************************************************************************************************************************/
                        //**********Step2: If true , get the Scanner Read value and Generate ItemID************
                        /************************************************************************************************************************/
                        if (reqScanFirstTime)
                        {
                            try
                            {
                                busyScanFirstTimeFlag = true;
                                IDReaderCommFunctions.ScannerBusyStatus = busyScanFirstTimeFlag;
                                Werkplaatsnummer = 0;
                                ProefstukUitnameCode = 0;
                                ProefIndex = 0;
                                IDReaderCommFunctions.ScannedString = "";
                                IDReaderCommFunctions.ScannerTriggered = "ON";
                                Thread.Sleep(5000);
                                //IDReaderCommFunctions.ScannerTriggered = "OFF";
                                if (IDReaderCommFunctions.ScannedString.Length != 0)
                                {
                                    IDReaderCommFunctions.ScannerTriggered = "OFF";
                                    Werkplaatsnummer = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(0, 4));
                                    ProefstukUitnameCode = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(4, 1));
                                    ProefIndex = Convert.ToInt32(IDReaderCommFunctions.ScannedString.Substring(5, 1));

                                    if (Werkplaatsnummer.ToString().Length == 4 && Werkplaatsnummer != 0 && ProefstukUitnameCode.ToString().Length == 1 && ProefIndex.ToString().Length == 1)
                                        ItemID = Convert.ToInt32(Werkplaatsnummer.ToString() + ProefstukUitnameCode.ToString() + ProefIndex.ToString());  // need to be generated based on the output from scanner

                                    /************************************************************************************************************************/
                                    //************Step3: Get other info from Tata side table with the ItemID  as key using WCF************
                                    /************************************************************************************************************************/
                                    if (ItemID != 0)
                                    {
                                        _KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag.proef_indexField = ProefIndex;
                                        _KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag.uitnamerichtingField = ProefstukUitnameCode;
                                        _KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag.werkplaatsidentificatieField = Werkplaatsnummer;

                                        //recTata = myService.GetRecords().ToList();  // Returns the whole table from Tata via WCF

                                        //First Fill Header for service
                                        _KerfslagBewerkingHeader.msg_IdentificationField = "KERFSLAGBEWERKINGAANVRAAG";
                                        _KerfslagBewerkingHeader.msg_SourceField = "CELLRO";
                                        _KerfslagBewerkingHeader.msg_DestinationField = "KLABBES";
                                        _KerfslagBewerkingHeader.msg_TimestampField = DateTime.Now;

                                        _KerfslagBewerkingAanvraagBericht.header_GegField = _KerfslagBewerkingHeader;
                                        _KerfslagBewerkingAanvraagBericht.kerfslagBewerkingAanvraagField = _KerfslagBewerkingAanvraagBerichtKerfslagBewerkingAanvraag;
                                        try
                                        {
                                            _KerfslagBewerkingResultaatBericht = _KerfslagBewerkingServiceClient.GeefBewerkingInstellingen(_KerfslagBewerkingAanvraagBericht);
                                        }
                                        catch (Exception ex)
                                        { scanOKFlag = false; WriteToEventLog("Communication with Tata service failed. Werkplaatsnummer: " + Werkplaatsnummer + " ProefStukUitnameCode: " + ProefstukUitnameCode + " ProefIndex: " + ProefIndex + " Exception: " + ex); }
                                        _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat = _KerfslagBewerkingResultaatBericht.kerfslagBewerkingResultaatField;

                                        //var query1 = from i in recTata
                                        //             where i.Werkplaatsnummer == Werkplaatsnummer && i.ProefstukUitnameCode == ProefstukUitnameCode && i.ProefIndex == ProefIndex
                                        //             select i;
                                        if (_KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat != null && _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.meldingsCodeField == "00")
                                        {
                                            /************************************************************************************************************************/
                                            //************Step4: Insert the data into the Local SampleData table at CellRO side, if already not available************
                                            /************************************************************************************************************************/
                                            //var rec = query1.First();

                                            var query2 = from i in db.SampleDatas
                                                         where i.Werkplaatsnummer == Werkplaatsnummer && i.ProefstukUitnameCode == ProefstukUitnameCode && i.ProefIndex == ProefIndex
                                                         select i;
                                            if (query2.Count() == 0) //if not available already in SampleData table and Status from Tata should be true
                                            {
                                                sd.ItemID = ItemID;
                                                sd.Werkplaatsnummer = Werkplaatsnummer;
                                                sd.ProefstukUitnameCode = ProefstukUitnameCode;
                                                sd.ProefIndex = ProefIndex;
                                                sd.Status = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.meldingsCodeField;
                                                sd.TolBoven = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.tolerantieBovenField;
                                                sd.TolOnder = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.tolerantieOnderField;
                                                sd.Orderdikte = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.keurplaatDikteField;
                                                sd.CodeRm = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.rmVerwachtField;
                                                sd.FreesDikte = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.freesMaatField;
                                                sd.BeproevingsTemperatuur = _KerfslagBewerkingResultaatBerichtKerfslagBewerkingResultaat.beproevingstemperatuurField;
                                                db.SampleDatas.InsertOnSubmit(sd);
                                                db.SubmitChanges();
                                                db = null;
                                            }

                                        }
                                        else { scanOKFlag = false; }
                                    }
                                    else { scanOKFlag = false; }
                                }
                                else
                                {
                                    scanOKFlag = false;
                                    IDReaderCommFunctions.ScannerTriggered = "OFF";
                                    WriteToEventLog("Time out.Not able to read string");
                                }
                            }
                            catch
                            {
                                scanOKFlag = false;
                                WriteToEventLog("Scanning or communication with Tata service failed");
                            }
                        }
                        //************************************************************************************************************************/
                        //********Step5: After successfull insert ,Update the AckOK bit in the table  and Update the ItemID,else update AckNOK***
                        /************************************************************************************************************************/
                        if (reqScanFirstTime && busyScanFirstTimeFlag)
                        {
                            if (scanOKFlag)
                            {
                                RobotSignals.AckScanFirstTimeOK = true;
                                RobotSignals.AckScanFirstTimeOK_ItemID = ItemID;
                                RobotSignals.LastScanned_ItemID = ItemID;
                            }
                            else
                                RobotSignals.AckScanFirstTimeNOK = true;
                        }

                        if (reqScanPrinted && RobotSignals.ReqScanPrinted_ItemID != 0)
                        {
                            //    //Scan the printed code; if datavalid then return OK; else return NOK.
                            try
                            {
                                //MessageBox.Show("inside scan print time");
                                busyScanPrintFlag = true;
                                IDReaderCommFunctions.ScannerBusyStatus = busyScanPrintFlag;
                                Werkplaatsnummer = 0;
                                ProefstukUitnameCode = 0;
                                ProefIndex = 0;
                                messageFromScanner = string.Empty;
                                messageFromDB = string.Empty;
                                ItemID = RobotSignals.ReqScanPrinted_ItemID;

                                IDReaderCommFunctions.ScannedString = "";
                                IDReaderCommFunctions.ScannerTriggered = "ON";
                                Thread.Sleep(5000);
                                if (IDReaderCommFunctions.ScannedString.Length != 0)
                                {

                                    IDReaderCommFunctions.ScannerTriggered = "OFF";
                                    messageFromScanner = IDReaderCommFunctions.ScannedString;

                                    /************************************************************************************************************************/
                                    //************Step3: Get info from Sample Data table with the ItemID as key************
                                    /************************************************************************************************************************/
                                    //var query1 = from i in db.SampleDatas
                                    //             where i.ItemID == ItemID
                                    //             select i;

                                    //if (query1.Count() != 0)
                                    //{

                                    //    messageFromDB = sd.ItemID.ToString();// get from DB

                                    //    if (messageFromDB != messageFromScanner)
                                    //    { scanOKFlag = false; }
                                    //}
                                    //else
                                    //{
                                    //    scanOKFlag = false;
                                    //}
                                    if (messageFromScanner != ItemID.ToString())
                                    { scanOKFlag = false; }
                                }
                                else
                                {
                                    IDReaderCommFunctions.ScannerTriggered = "OFF";
                                    scanOKFlag = false;
                                    WriteToEventLog("Time out.Not able to read string");
                                }

                            }
                            catch (Exception ex)
                            {
                                scanOKFlag = false;
                                WriteToEventLog("Thrown exception in ReqScanPrinted: " + ex);
                            }
                        }

                        if (reqScanPrinted && busyScanPrintFlag)
                        {
                            if (scanOKFlag) RobotSignals.AckScanPrintedOK = true;
                            else RobotSignals.AckScanPrintedNOK = true;
                        }
                        db = null;
                    }
                    Thread.Sleep(500);
                }
                sd = null;
            }
            catch (Exception ex)
            {
                WriteToEventLog("Thrown exception in Scannercycle: " + ex);
            }

        }

        public static void PrinterCycle()
        {
            try
            {
                ////Print from SampleData where Item_ID = ItemID with postfix "-1", "-2", or "-3" for each subpart            
                DBContextDataContext db = new DBContextDataContext();   // CellRODB Database connection

                bool exitCondition = false;
                bool exitCondition2 = false;
                bool busyFlag = false;
                bool reqPrint = false;
                bool _ReqPrint = false;
                bool _AckPrintOK = false;
                bool _AckPrintNOK = false;
                bool _LastDotMarked = false;
                bool _PrinterErrorStatus = false;
                bool printOKFlag = true;

                string message = string.Empty;
                string data = string.Empty;
                int Werkplaatsnummer = 0;
                int ProefstukUitnameCode = 0;
                int ProefIndex = 0;
                double Dikte = 0;
                int Zas = 0;
                int FirstPosX = 0;
                int SecondPosX = 0;
                int ThirdPosX = 0;
                int FirstPosY = 0;
                int SecondPosY = 0;
                int FirstPosZ = 0;
                double Difference;

                while (AutomatorCommFunctions.PrinterStarted)
                {
                    exitCondition = AutomatorCommFunctions.PrinterPaused;
                    if (!exitCondition)
                    {
                        /************************************************************************************************************************/
                        //********Step1. Check if ReqPrint bit is High and resp. Ack Bits are low.*****************
                        /************************************************************************************************************************/

                        AutomatorCommFunctions.PrinterBusyStatus = busyFlag;


                        reqPrint = false;
                        _ReqPrint = RobotSignals.ReqPrint;
                        _AckPrintOK = RobotSignals.AckPrintOK;
                        _AckPrintNOK = RobotSignals.AckPrintNOK;
                        _LastDotMarked = AutomatorCommFunctions.LastDotMarked;
                        _PrinterErrorStatus = AutomatorCommFunctions.PrinterErrorStatus;
                        printOKFlag = true;

                        //if (!_ReqPrint && busyFlag) busyFlag = false;
                        if (_ReqPrint && !_AckPrintOK && !_AckPrintNOK)
                        { if (!(bool)busyFlag) reqPrint = true; }
                        else
                        {
                            if (!_ReqPrint && (_AckPrintOK || _AckPrintNOK))
                            {
                                RobotSignals.AckPrintOK = false;
                                RobotSignals.AckPrintNOK = false;
                                busyFlag = false;
                                ////May be need to nullify the ItemID in the CnxnReqData along ReqPrint
                            }
                        }

                        /************************************************************************************************************************/
                        //**********Step2: Get the ItemID and find relevant info in SampleData and generate the message string************
                        /************************************************************************************************************************/
                        if (reqPrint || RetryReqPrint)
                        {
                            busyFlag = true;
                            RetryReqPrint = false;
                            AutomatorCommFunctions.PrinterBusyStatus = busyFlag;
                            int ItemID = 0;
                            ItemID = RobotSignals.ReqPrint_ItemID;

                            try
                            {
                                if (ItemID != 0)
                                {
                                    EventLogger.EventLog("Try to print, ItemID = " + ItemID);
                                    message = string.Empty;
                                    data = string.Empty;
                                    Werkplaatsnummer = 0;
                                    ProefstukUitnameCode = 0;
                                    ProefIndex = 0;

                                    var query1 = from i in db.SampleDatas
                                                 where i.ItemID == ItemID
                                                 select i;

                                    if (query1.Count() != 0)
                                    {
                                        var rec = query1.First();
                                        Werkplaatsnummer = rec.Werkplaatsnummer;
                                        ProefstukUitnameCode = rec.ProefstukUitnameCode;
                                        ProefIndex = rec.ProefIndex;

                                        var query2 = from i in db.Variables
                                                     select i;

                                        foreach (var i in query2)
                                        {
                                            if (i.VariableName.Contains("PrinterXPos"))
                                            {
                                                FirstPosX = Convert.ToInt16(i.Value);
                                                SecondPosX = FirstPosX + 205;
                                                ThirdPosX = FirstPosX + 410;
                                            }
                                            if (i.VariableName.Contains("PrinterYPos"))
                                            {
                                                FirstPosY = Convert.ToInt16(i.Value);
                                                SecondPosY = FirstPosY + 85;
                                            }
                                            if (i.VariableName.Contains("PrinterHeight"))
                                            {
                                                FirstPosZ = Convert.ToInt16(i.Value);
                                                Dikte = Convert.ToDouble(rec.FreesDikte);

                                                //difference (Max(10) and freesdikte)*10
                                                Difference = (10 - Dikte) * 10;
                                                Zas = FirstPosZ + Convert.ToInt16(Difference);
                                                //if (Dikte == 5) { Zas = FirstPosZ + 50; }
                                                //if (Dikte == 7.5) { Zas = FirstPosZ + 25; }
                                                //if (Dikte == 10) { Zas = FirstPosZ; }
                                            }
                                        }

                                        //Dikte = Convert.ToDouble(rec.FreesDikte);
                                        //if (Dikte == 5) { Zas = 470; }
                                        //if (Dikte == 7.5) { Zas = 445; }
                                        //if (Dikte == 10) { Zas = 420; }

                                        data = Werkplaatsnummer.ToString() + ProefstukUitnameCode.ToString() + ProefIndex.ToString();  //need to be modified 

                                        /* NEWFILE <Marking speed> <Fast speed> <Crossed zero> <File name> [CR][LF]    
                                           INSERTTEXTLINE <X> <Y> <Z> <W> <H> <Angle> <Radius> <Space> <Force> <Quality> <Text> [CR][LF]
                                           X, Y, Z Coordonate of the line, in tenth of mm
                                           W, H Width and height of the chars, in tenth of mm
                                           Angle In hundredth of degrees from –18000 To 18000
                                           Radius In tenth of mm
                                           Space Space between chars ( from 0 To 50 )
                                           Force From 0 To 9
                                           Quality From 1 To 9
                                           Text Text To be marked  */
                                        message = "FILEDELETE CELLRO1 2\n" +
                                                    "NEWFILE 5 7 0 CELLRO1\n" +
                                                    "INSERTECC200LINE " + FirstPosX + " " + FirstPosY + " " + Zas + " 70 70 -9000 0 5 0 3 " + data + "\n" +
                                                    "INSERTTEXTLINE " + (FirstPosX + 45) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + Werkplaatsnummer + "\n" +
                                                    "INSERTTEXTLINE " + (FirstPosX + 5) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + ProefstukUitnameCode + ProefIndex + "-1\n" +
                                                    "INSERTECC200LINE " + SecondPosX + " " + FirstPosY + " " + Zas + " 70 70 -9000 0 5 0 3 " + data + "\n" +
                                                    "INSERTTEXTLINE " + (SecondPosX + 45) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + Werkplaatsnummer + "\n" +
                                                    "INSERTTEXTLINE " + (SecondPosX + 5) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + ProefstukUitnameCode + ProefIndex + "-2\n" +
                                                    "INSERTECC200LINE " + ThirdPosX + " " + FirstPosY + " " + Zas + " 70 70 -9000 0 5 0 3 " + data + "\n" +
                                                    "INSERTTEXTLINE " + (ThirdPosX + 45) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + Werkplaatsnummer + "\n" +
                                                    "INSERTTEXTLINE " + (ThirdPosX + 5) + " " + SecondPosY + " " + Zas + " 17 17 -9000 0 5 5 4 " + ProefstukUitnameCode + ProefIndex + "-3\n" +
                                                    "SAVEFILE CELLRO1 \n" +
                                                    "RUN \n";
                                        /************************************************************************************************************************/
                                        //**********Step3: Get the message string to be passed to the Automator and it reurns true on successful ************
                                        /************************************************************************************************************************/
                                        AutomatorCommFunctions.PrintMessage(message);

                                        if (FirstTime == false)
                                        {
                                            //timer1.Interval = 35000; // here time in milliseconds
                                            //timer1.Tick += timer_Tick;
                                            timer1.Elapsed += new ElapsedEventHandler(timer_Tick);
                                            FirstTime = true;
                                        }
                                        timer1.Start();
                                    }
                                    else
                                    { printOKFlag = false; } WriteToEventLog("SampleData doesn't have a matching record");
                                }
                                else
                                {
                                    RetryReqPrint = true;
                                    EventLogger.EventLog("RetryReqPrint is Set!!! (No ItemID found, try again)");
                                }
                            }
                            catch (Exception ex)
                            {
                                printOKFlag = false;
                                MessageBox.Show("Thrown exception in reqPrint: " + ex);
                                //throw;
                            }
                        }
                        if (printOKFlag)
                        {
                            if (!reqPrint && busyFlag)
                            {
                                //************************************************************************************************************************/
                                //********Step4: After successfull print ,Update the AckOK bit in the table ,else update AckNOK***
                                /************************************************************************************************************************/
                                //logger.MyLogFile(RobotSignals.ReqPrint_ItemID + " LastDotMarked = '" + _LastDotMarked, "' Error(Tijdens of Na) = '" + _PrinterErrorStatus + "'");
                                if (_PrinterErrorStatus || _LastDotMarked)
                                {
                                    EventLogger.EventLog(RobotSignals.ReqPrint_ItemID + " LastDotMarked = '" + _LastDotMarked + "' Error(Tijdens of Na) = '" + _PrinterErrorStatus + "'");
                                }

                                if (_PrinterErrorStatus)
                                {
                                    AutomatorCommFunctions.PrintMessage("RESETERROR \n");
                                    AutomatorCommFunctions.PrintMessage("HOMEPOSITION <Motors> \n");
                                    RobotSignals.AckPrintNOK = true; busyFlag = false;
                                    //if (_LastDotMarked)
                                    //{
                                    //    RobotSignals.AckPrintNOK = true; busyFlag = false;
                                    //}
                                    AutomatorCommFunctions.LastDotMarked = false;
                                    AutomatorCommFunctions.PrinterErrorStatus = false;
                                }

                                if (_LastDotMarked && !_PrinterErrorStatus)
                                {
                                    RobotSignals.AckPrintOK = true; busyFlag = false;
                                    AutomatorCommFunctions.LastDotMarked = false;
                                    AutomatorCommFunctions.PrinterErrorStatus = false;
                                }
                            }
                        }
                        else
                        {
                            EventLogger.EventLog(RobotSignals.ReqPrint_ItemID + "SampleData bestaat niet of error tijdens aanmaken/versturen printprogramma");
                            RobotSignals.AckPrintNOK = true;
                        }

                    }
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                WriteToEventLog("Thrown exception in Printercycle: " + ex);
            }
        }

        public static void ReqDeleteItem()
        {
            //Delete from SampleData where Item_ID = ItemID

            DBContextDataContext db = new DBContextDataContext();
            bool deleteFlag = false;
            bool _ReqDeleteItem = RobotSignals.ReqDeleteItem;
            bool _AckDeleteItem = RobotSignals.AckDeleteItemOK;
            int _ReqDeleteItem_ItemID = RobotSignals.ReqDeleteItem_ItemID;

            if (_ReqDeleteItem && !_AckDeleteItem)
            {
                if (_ReqDeleteItem_ItemID != 0)
                {
                    try
                    {
                        var Query = from i in db.SampleDatas
                                    where i.ItemID == _ReqDeleteItem_ItemID
                                    select i;
                        if (Query.Count() != 0)
                        {
                            foreach (var ID in Query)
                            {
                                db.SampleDatas.DeleteOnSubmit(ID);
                            }
                            db.SubmitChanges();
                        }
                        deleteFlag = true;

                    }
                    catch (Exception ex)
                    {
                        deleteFlag = false;
                        WriteToEventLog("Failed to delete ItemID, exception: " + ex);
                    }
                }
                deleteFlag = true;
            }
            else
                if (!_ReqDeleteItem && _AckDeleteItem) RobotSignals.AckDeleteItemOK = false;

            if (deleteFlag) RobotSignals.AckDeleteItemOK = true;
        }

        public static void ReqDeleteAll()
        {
            //Truncate SampleData
            DBContextDataContext db = new DBContextDataContext();
            bool deleteFlag = false;
            bool _ReqDeleteAll = RobotSignals.ReqDeleteAll;
            bool _AckDeleteAll = RobotSignals.AckDeleteAllOK;

            if (_ReqDeleteAll && !_AckDeleteAll)
            {
                try
                {
                    var Query = from i in db.SampleDatas
                                select i;

                    foreach (var ID in Query)
                    {
                        db.SampleDatas.DeleteOnSubmit(ID);
                    }
                    db.SubmitChanges();
                    deleteFlag = true;

                }
                catch (Exception ex)
                {
                    deleteFlag = false;
                    WriteToEventLog("Failed to delete all ItemID's, exception: " + ex);
                }
            }
            else
                if (!_ReqDeleteAll && _AckDeleteAll) RobotSignals.AckDeleteAllOK = false;

            if (deleteFlag) RobotSignals.AckDeleteAllOK = true;
        }

        public static void ReqLogMeasuredValue()
        {
            //Log measured value
            bool LoggedFlag = false;
            bool _ReqLogging = RobotSignals.ReqReadMeasuredValue;
            bool _AckLogging = RobotSignals.AckReadMeasuredValue;

            if (_ReqLogging & !_AckLogging)
            {
                try
                {
                    //File management       //File per day, files older then 10 days are deleted!   
                    int ColumnWidth = 16;
                    int ColumnWidthDateTime = 20;
                    double RobotTol = 0.2;
                    string StandardHeader = "Datum/Tijd".PadRight(ColumnWidthDateTime) + "Code".PadRight(ColumnWidth) + "Gevr.Dikte".PadRight(ColumnWidth) + "Tol.Onder".PadRight(ColumnWidth) + "Tol.Boven".PadRight(ColumnWidth) + "Tol.Robot".PadRight(ColumnWidth) + "Meet dikte".PadRight(ColumnWidth) + "Binnen Tolerantie".PadRight(ColumnWidth);
                    string FileName;
                    string FilePath = @"D:\MeasuredLogs\";
                    string Name = "Measured_";
                    string Extension = ".txt";
                    int Year = DateTime.Now.Year;
                    int Month = DateTime.Now.Month;
                    int Day = DateTime.Now.Day;
                    string MonthZero;
                    string DayZero;
                    string Data;

                    //if (Month < 10) MonthZero = "0";
                    //else MonthZero = "";
                    //if (Day < 10) DayZero = "0";
                    //else DayZero = "";

                    //Check active day file exists
                    FileName = Name + Year + Extension;                        //DayZero + Day + MonthZero + Month + Year + Name + Extension;
                    //Select data (database + robotdata)
                    var LocalDatetime = DateTime.Now.ToString("dd-MM-yyyy HH:mm");            //new DateTime(Src.Year, Src.Month, Src.Day, Src.Hour, Src.Minute); 
                    int LocalItemID = RobotSignals.ItemID; //"123456".PadRight(ColumnWidth);       //RobotSignals.LastScanned_LocalItemID
                    double LocalAskedThickness = RobotSignals.AskedThickness;//12;       //Database/Remembered values
                    double LocalTolUnder = RobotSignals.TolUnder; //0.6;    //DataBase/Remembered values
                    double LocalTolAbove = RobotSignals.TolAbove;  //0.7;     //Database/Remembered values
                    double LocalMeasThickness = RobotSignals.ReqReadMeasuredValue_Value; //12.70;    //MeasuredValue
                    string OKNOK;       //determined if its between tolerance

                    if (((LocalAskedThickness - (LocalTolUnder + RobotTol)) < LocalMeasThickness) & (LocalMeasThickness < (LocalAskedThickness + (LocalTolAbove + RobotTol))))
                    {
                        OKNOK = "Ja";
                    }
                    else
                    {
                        OKNOK = "Nee";
                    }

                    Data = LocalDatetime.ToString().PadRight(ColumnWidthDateTime) + LocalItemID.ToString().PadRight(ColumnWidth) + LocalAskedThickness.ToString().PadRight(ColumnWidth) + LocalTolUnder.ToString().PadRight(ColumnWidth) + LocalTolAbove.ToString().PadRight(ColumnWidth) + RobotTol.ToString().PadRight(ColumnWidth) + LocalMeasThickness.ToString().PadRight(ColumnWidth) + OKNOK.ToString().PadRight(ColumnWidth);

                    if (!File.Exists(FilePath + FileName))
                    {
                        //Create File
                        File.AppendAllText((FilePath + FileName), StandardHeader + Environment.NewLine);
                        ;                    ////check if more then 10 files exists, then delete oldest (17-05-2018 Client doesn't want to delete old files, take care of themselves)
                                             //foreach (var fi in new DirectoryInfo(FilePath).GetFiles().OrderByDescending(x => x.LastWriteTime).Skip(10))
                                             //    fi.Delete();
                    }

                    //Append line with data
                    File.AppendAllText((FilePath + FileName), Data + Environment.NewLine);
                    RobotSignals.AckReadMeasuredValue = true;
                    LoggedFlag = true;

                }
                catch (Exception ex)
                {
                    LoggedFlag = false;
                    WriteToEventLog("Failed to Read/log measured value, exception: " + ex);
                }
            }
            else
                if (!_ReqLogging && _AckLogging) RobotSignals.AckReadMeasuredValue = false;

            if (LoggedFlag) RobotSignals.AckReadMeasuredValue = true;
        }
    }
}